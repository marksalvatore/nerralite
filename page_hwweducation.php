<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: HWW Education
 *
 */


remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_hwweducation_overview');
add_action('genesis_loop', 'nerra_display_hwweducation_issueblocks');
add_filter( 'body_class','nerra_addclass_howwework' );

function nerra_display_hwweducation_overview() {
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1></header>
						<div class='text'>";
							the_content();
			echo "</div>";
		echo "</div>
					<div class='image'>";	
						the_post_thumbnail( 'full' );
		echo "</div>";
		echo "</section>";

	endwhile;
}
function nerra_display_hwweducation_issueblocks() {
	
	$stories = array( 
				array ( 'page_id' => '1312', 'title' => 'Teachers on the estuary = climate science in the classroom'),
				array ( 'page_id' => '1315', 'title' => 'Citizen science powers Alaska’s Kachemak reserve'),
				array ( 'page_id' => '1320', 'title' => 'Virginia reserve boosts local awareness of climate change'),
				array ( 'page_id' => '1325', 'title' => 'Hands-on training inspires STEM teaching in New Jersey'),
			);

	// array of pageids for the 4 projects you want to feature
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));
	$story_images = array ( 'HWW_Education_1_tn.png', 'HWW_Education_2_tn.png', 'HWW_Education_3_tn.png', 'HWW_Education_4_tn.png' );
	
	echo "<section class='blocks'>";
		echo "<div class='group'>";
		
			echo "<div class='one-fifth first'>";	
				echo "<div class='hottopic'>
								<div class='title'>
									Education
									<h4>featured projects</h4>
									<a class='link-indicator' href='index.php?page_id=1330'>Browse</a>
								</div>
									<div class='image'>
										<img src='$path_to_resources/images/site/HWW_FeaturedProjects_Education.png' alt='Featured projects: Education'>
									</div>
								</div>";
			
			echo "</div><div class='four-fifths'>"; 
		
		$i=0;
		
		foreach ( $stories as $story ) {	
			
			if ( 0 == $i ) { 
					echo "<div class='one-fourth first'>"; 
			}	else {
					echo "<div class='one-fourth'>"; 				
			}
			echo "<div class='issue'>";
					echo "<a href='index.php?page_id=".$story['page_id']."'><div class='title'>".$story['title']."</div>";
					echo "<div class='image'><img src='$path_to_resources/images/site/$story_images[$i]' width='210' height='162' alt='".$story['title']."'></div></a>";
			echo "</div>";
			
			echo "</div>"; //.one-fourth
			$i++;
		}	
		echo "</div>"; //.four-fifths
		
	echo "</div></section>"; //.group .blocks

}


genesis();


