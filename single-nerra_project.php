<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Single Post Template: NERRA Project
* Description: Custom page
*/

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_project_page');
add_filter( 'body_class','nerra_addclass_howwework' );

function nerra_display_project_page() {
	
	$template_path = dirname(get_bloginfo('stylesheet_url'));
	
	while ( have_posts() ) : the_post();
				
		$title = get_the_title();
		$description = get_field('project_description');
		$storymap_url = get_field('project_story_map_url');
		$impact_text = get_field('project_impact_text');
		$project_image = get_field('project_image');
		$howitworked = get_field('project_how_it_worked');
		$toolkit_text = get_field('project_toolkit_text');
		$toolkit_link = get_field('project_toolkit_case_study_url');
		$resources_text = get_field('project_resources_text');
		$partners_text = get_field('project_partners_text');
		$contacts_text = get_field('project_contacts_text');
		$project_permalink = get_permalink();


		// Overview
		echo "<section class='overview'>
						<div class='group'>
							<header><h1>$title</h1></header>
								<ul class='jump-menu'>									
									<li><a href='#impact'>Project impact</a></li>
									<li><a href='#howitworked'>How it worked</a></li>
									<li><a href='#resources'>Resources</a></li>
									<li><a href='#partners'>Partners</a></li>
									<li><a href='#contact'>Contact</a></li>
								</ul>									
								<div class='text'>$description</div>";
			echo "</div>"; // .group
			echo "<div class='image'>
							<img src=".$project_image['url']." alt='$title' />
							<div class='navette'><span class='arrow-bullet'></span><a class='link-indicator' href='/how-we-work/collaborative-research/'>Browse more collaborative research projects</a></div>";
								// story map icon
								if ( !empty($storymap_url) ) {
									echo "<div class='storymap'>
													<div class='storymap-icon'>
														<img src='$template_path/images/site/MagnifyingLensSolo.png' width='95' height='96' alt='Story map: $title'>
													</div>
													<div class='storymap-text'>
														<p>EXPLORE<br />THIS PROJECT</p>
														<div><a class='link-indicator' href='$storymap_url' target='_blank'>view storymap</a></div>
													</div>
												</div>"; // .storymap
								}	
			echo "</div>"; // .image
						
				
		echo "</section><!-- .overview -->";
			
			
			// IMPACT & QUOTE
			echo "<span id='impact'></span>";
			echo "<div class='project-impact'>";
				echo "<h2>Project Impact</h2>";
				echo "<div class='project-impact-text three-fifths first'>";
								echo $impact_text;
				echo "</div>";
				
				echo "<div class='project-quote two-fifths'>";
					echo "<div class='project-quote-image one-half first'>";
					
						echo "<div class='project-quote-text one-half'>";
		
						echo "</div>"; // .project-quote-text 
				echo "</div>"; // .project-quote
			echo "</div>"; // .project-impact
			
			echo "<div class='hr'></div>";

	
			// HOW IT WORKED
			echo "<span id='howitworked'></span>";
			echo "<div class='project-howitworked'>";
				echo "<h2>How it worked</h2>";
				echo "<div class='project-howitworked-text three-fifths first'>$howitworked</div>";
				echo "<div class='project-howitworked-image two-fifths'>";
					if ( strlen( $toolkit_text ) ) {
						echo "<a href='/how-we-work/collaborative-project-toolkit'>";
							echo "<img src='$template_path/images/site/CaseStudy_Top.png' width='352' height='125' alt='NERRA Collaboration Research Guide' />";
						echo "</a>";
						echo "<div class='casestudy-callout'>";
							echo "<p>$toolkit_text</p>";
							
							if ( ! empty($toolkit_link)) {
									echo "<a href='$toolkit_link#casestudies'>";
							} else {
									echo "<a href='/how-we-work/collaborative-project-toolkit/'>";							
							}
														
							echo "<img src='$template_path/images/site/CaseStudy_Button.png' width='308' height='39' alt='NERRA Collaboration Research Guide' /></a>";						
						echo "</div>";
					}
				echo "</div>"; // project-howitworked-image
			echo "</div>"; // .project-howitworked

			echo "<div class='hr'></div>";
			

			// RESOURCES
			echo "<span id='resources'></span>";
			echo "<div class='project-resources'>";
			echo "<h2>Resources</h2>";
			echo "<div class='project-resources-text three-fifths first'>$resources_text</div>";
			echo "<div class='two-fifths'></div>";
			echo "<ul class='three-fifths first'>";
			$args = array(
				'post_type' 			=> 'project_resource', // here's the magic
				'orderby'   			=> 'meta_value_num',
				'meta_key'  			=> 'project_resource_sort_order',
				'order'         	=> 'asc',
				'posts_per_page' 	=> -1,
			);
			
			$loop_resources = new WP_Query( $args );
			
			if( $loop_resources->have_posts() ) {
		
				while( $loop_resources->have_posts() ) : $loop_resources->the_post();
						
					$resource_title = get_the_title();					
					$resource_url = get_field('project_resource_url');
					$resource_file = get_field('project_resource_file');
					$resource_projectname = get_field('project_resource_projectname'); // page link field from ACF

					if( $project_permalink == $resource_projectname) { // the 'page link' field in ACF returns the permalink for that page 

						if ( strlen( $resource_url ) ) {
							echo "<li><a href='$resource_url' target='_blank'>$resource_title</a></li>";
						} else {
							echo "<li>$resource_title <a href='$resource_file[url]'>view</a></li>";						
						}
					}
	
				endwhile;
				
				echo "</ul>";
				//echo "<div class='two-fifths'></div>";
				
				echo "</div>"; // .project-resources
				

				echo "<div class='hr'></div>";
			}


			// PARTNERS
			echo "<span id='partners'></span>";
			echo "<div class='project-partners'>";
			echo "<h2>Partners</h2>";
			echo "<div class='three-fifths first'>";
				echo $partners_text;
			echo "</div>"; // three-fifths
			echo "<div class='two-fifths'></div>";
			echo "</div>"; // .project-partner
			
			echo "<div class='hr'></div>";

			// CONTACT
			echo "<span id='contact'></span>";
			echo "<div class='project-contact'>";
			echo "<h2>Contact</h2>";
			echo $contacts_text;
			echo "</div>";
			
	endwhile;

}


genesis();


