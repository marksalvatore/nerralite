<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: CPToolkit Planning
 *
 */


remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_hwwtoolkit');
add_filter( 'body_class','nerra_addclass_howwework' );
add_filter( 'body_class','nerra_addclass_page_toolkit' );

function nerra_display_hwwtoolkit() {
	
	while ( have_posts() ) : the_post();
	
		$title = get_the_title();
		
		// Overview
		echo "<section class='overview'>
						<header>
							<h1>Planning</h1>
						</header>";
			echo "<div class='image'>";	
				echo "<a href='/how-we-work/collaborative-project-toolkit/'><img src='/wp-content/uploads/2015/06/Toolkit_ToolboxBig.png' alt='Collaborative Project Toolkit'></a>";	
			echo "</div>";
		echo "</section>";
	
		echo "<div class='overview-text'>";
		
		if ( ! is_page('planning') ) {
			echo "<h2>$title</h2>";
		}
						
		the_content(); 

		if ( ! is_page('planning') ) {
			// jump links
			nerra_display_toolkit_jumplinks();;
		}

		echo "</div>"; // overview-text
	
	endwhile;
	
	// Each subsection has a "termid" used to query 
	if ( is_page('frame-your-project') )   {
		nerra_display_toolkit_frameyourproject();
	}
	if ( is_page('build-your-team') )   {
		nerra_display_toolkit_buildyourteam();
	}
	if ( is_page('choose-your-approach') )   {
		nerra_display_toolkit_chooseyourapproach();
	}
	if ( is_page('budget-for-success') )   {
		nerra_display_toolkit_budgetforsuccess();
	}
	
}

function nerra_display_toolkit_frameyourproject(){
	
	$termid = 87; // category id for this subsection
	
	echo "<div class='hr'></div>";
	echo "<div class='toolkit-content'>";
	
		echo "<h3 id='bestpractices'>Best Practices</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'best practice'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
					
		echo "<div class='hr'></div>";
		
		echo "<h3 id='casestudies'>Case Studies</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'case study'; // document toolkit type
					$loop = nerra_get_documents_by_termid(87); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

		echo "<div class='hr'></div>";
		
		echo "<h3 id='resources'>Resources</h3>";
		echo "<div class='intro-text'>";
		echo "Tools  and references in this section include information on how to develop and use a logic model, 
					a timeline to help with project planning, and a reference for a handbook on public participation in 
					collaborative projects.";
		echo "</div>"; //.intro-text
					$doctype = 'resource'; // document toolkit type
					$loop = nerra_get_documents_by_termid(87); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

		
	echo "</div>"; // .toolkit-content	
}
function nerra_display_toolkit_buildyourteam(){
	
	$termid = 88; // category id for this subsection
	
	echo "<div class='hr'></div>";
	echo "<div class='toolkit-content'>";
	
		echo "<h3 id='bestpractices'>Best Practices</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'best practice'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
					
		echo "<div class='hr'></div>";
		
		echo "<h3 id='casestudies'>Case Studies</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'case study'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

		echo "<div class='hr'></div>";
		
		echo "<h3 id='resources'>Resources</h3>";
		echo "<div class='intro-text'>";
		echo "Resources in this section can help you articulate the skills you need for your 
					collaborative lead, understand team dynamics, and develop a team charter.";
		echo "</div>"; //.intro-text
					$doctype = 'resource'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

	echo "</div>"; // .toolkit-content	
	
}
function nerra_display_toolkit_chooseyourapproach(){
	
	$termid = 89; // category id for this subsection
	
	echo "<div class='hr'></div>";
	echo "<div class='toolkit-content'>";
	
		echo "<h3 id='bestpractices'>Best Practices</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'best practice'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
					
		echo "<div class='hr'></div>";
		
/*	No case studies at this time	
		echo "<h3 id='casestudies'>Case Studies</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'case study'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
		echo "<div class='hr'></div>";
*/
		
		echo "<h3 id='resources'>Resources</h3>";
		echo "<div class='intro-text'>";
		echo "Tools and citations in this section can help you learn more about the seven most common 
					approaches to collaboration. It also offers examples of the role of collaboration in a 
					variety of natural resource projects.";
		echo "</div>"; //.intro-text
					$doctype = 'resource'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

	echo "</div>"; // .toolkit-content	
	
}
function nerra_display_toolkit_budgetforsuccess(){
	
	$termid = 90; // category id for this subsection
	
	echo "<div class='hr'></div>";
	echo "<div class='toolkit-content'>";
	
		echo "<h3 id='bestpractices'>Best Practices</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'best practice'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
					
		echo "<div class='hr'></div>";
		
/*	No case studies at this time	
		echo "<h3 id='casestudies'>Case Studies</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'case study'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
		echo "<div class='hr'></div>";
*/
		
		echo "<h3 id='resources'>Resources</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'resource'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

	echo "</div>"; // .toolkit-content	
	
}



genesis();