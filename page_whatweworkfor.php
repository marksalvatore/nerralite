<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Template Name: What We Work For
* Description: A custom template
*/

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_whatweworkfor_overview');
add_action('genesis_loop', 'nerra_display_whatweworkfor_issueblocks');
add_filter( 'body_class','nerra_addclass_whatweworkfor' );
add_filter( 'body_class','nerra_addclass_page_primary' );

function nerra_display_whatweworkfor_overview() {
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1></header>
						<div class='text'><p>";
							the_content();
			echo "</p></div>";
		echo "</div>
					<div class='image'>";	
						the_post_thumbnail( 'full' );
		echo "</div>";
		echo "</section>";

	endwhile;
}

function nerra_display_whatweworkfor_issueblocks() {
	
	$menu_items = wp_get_nav_menu_items( 'What We Work For' );
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));
	$issue_images = array ( 'WWWF_Prepared_pic.png', 'WWWF_Healthy_pic.png', 'WWWF_CleanWater_pic.png', 'WWWF_Informed_pic.png' );
	
	echo "<section class='blocks-area'><div class='blocks'>";
	
		echo "<div class='one-fifth first'>";
	
			echo "<div class='hottopic'>";
			
		//* TEMPORARILY removing hottopic from display per DL 08/19/2015
				echo "<img src='$path_to_resources/images/site/HotTopicsSun_Large.png' width='157' height='88' alt='Hot Topics'>";
				//echo "<a href='#'>";
				echo	"<div class='caption'>Hot Topics</div>";
				//echo	"<div class='readmore'>Read more</div>";
				echo	"<div class='readmore'>Coming soon!</div>";
				//echo "</a>";
		//*/		
				echo "</div>";
		
		echo "</div><div class='four-fifths'>"; 
		
		$i=0;
		
		foreach ( $menu_items as $item ) {	
			if ( 0 == $i ) { 
					echo "<div class='one-fourth first'>"; 
			}	else {
					echo "<div class='one-fourth'>"; 				
			}
			echo "<div class='issue'>";
					echo "<a href='$item->url'><div class='title'>$item->title</div>";
					echo "<div class='image'><img src='$path_to_resources/images/site/$issue_images[$i]' width='206' height='256' alt='Emerging Hot Topics'></div></a>";
			echo "</div>";
			
			echo "</div>"; //.one-fourth
			$i++;
		}	
		echo "</div>"; //.four-fifths
		
	echo "</div></section>"; //.blocks .blocks-area 


}





genesis();


