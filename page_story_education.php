<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: Story Page Education
 *
 */

// This template is identical to the future and training story pages.
// I duplicated them only so I could have the css set the menu item to on.

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_story');
add_filter( 'body_class','nerra_addclass_howwework' );

function nerra_display_story() {
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1></header>";
			echo "<div class='text'>";
							the_content();
			echo "</div>";
		echo "</div>
					<div class='image'>";	
						the_post_thumbnail( 'full' );
		echo "<div class='navette'><span class='arrow-bullet'></span><a class='link-indicator' href='/how-we-work/education-for-an-informed-public/education-featured-projects/'>Browse more education stories</a></div>";
		echo "</div>";
		echo "</section>";

	endwhile;
}




genesis();


