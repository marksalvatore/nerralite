<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: HWW Future
 *
 */
 

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_hwwfuture_overview');
add_action('genesis_loop', 'nerra_display_hwwfuture_issueblocks');
add_filter( 'body_class','nerra_addclass_howwework' );

function nerra_display_hwwfuture_overview() {
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1></header>
						<div class='text'>";
							the_content();
			echo "</div>";
		echo "</div>
					<div class='image'>";	
						the_post_thumbnail( 'full' );
		echo "</div>";
		echo "</section>";

	endwhile;
}
function nerra_display_hwwfuture_issueblocks() {
	
	// Array of pageids for the 4 featured projects
	$stories = array( 
				array ( 'page_id' => '1651', 'title' => 'Lessons from a superstorm prepare Mainers for the future'),
				array ( 'page_id' => '1659', 'title' => 'Farming a return to the wild at Elkhorn Slough'),
				array ( 'page_id' => '1661', 'title' => 'Information to manage a changing bay'),
				array ( 'page_id' => '1665', 'title' => 'Blue Carbon heats up in the Pacific Northwest'),
			);

	// Cheating here. I should use a query to get these from the db
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));
	$story_images = array ( 'HWW_Future_1_tn.png', 'HWW_Future_2_tn.png', 'HWW_Future_3_tn.png', 'HWW_Future_4_tn.png' );
	
	echo "<section class='blocks'>";
		echo "<div class='group'>";
		
			echo "<div class='one-fifth first'>";	
				echo "<div class='hottopic'>
								<div class='title'>
										Land Protection
									<h4>featured projects</h4>
									<a class='link-indicator' href='index.php?page_id=1855'>Browse</a>
								</div>
									<div class='image'>
										<img src='$path_to_resources/images/site/HWW_FeaturedProjects_LandProtect.png' alt='Featured projects: Land protection'>
									</div>
								</div>";
			
			echo "</div><div class='four-fifths'>"; 
		
		$i=0;
		
		foreach ( $stories as $story ) {	
			
			if ( 0 == $i ) { 
					echo "<div class='one-fourth first'>"; 
			}	else {
					echo "<div class='one-fourth'>"; 				
			}
			echo "<div class='issue'>";
					echo "<a href='index.php?page_id=".$story['page_id']."'><div class='title'>".$story['title']."</div>";
					echo "<div class='image'><img src='$path_to_resources/images/site/$story_images[$i]' width='210' height='162' alt='".$story['title']."'></div></a>";
			echo "</div>";
			
			echo "</div>"; //.one-fourth
			$i++;
		}	
		echo "</div>"; //.four-fifths
		
	echo "</div></section>"; //.group .blocks

}


genesis();


