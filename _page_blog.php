<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: NERRA News
 *
 */

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_nerra_blog_page');

function nerra_display_nerra_blog_page() {

	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));

	$args = array(
		'meta_key'		=> 'blog_date', // use meta_key for custom fields
		'orderby'			=> 'meta_value',
		'order'				=> 'desc',
		'post_type' 	=> 'blog_news', // here's the magic
	);
	$loop = new WP_Query( $args ); ?>

		<section class='overview'>
			<div class='group'>
				<header><h1>The NERRA Blog</h1></header>
			</div>
			<div class='image'>
				<img src='<?php echo $path_to_resources;?>/images/site/News_Sunset.png' width='352' height='148' alt='The NERRA Blog'>
			</div>
		</section>


		<?php
		if( $loop->have_posts() ) {

			echo "<div class='blog-content'>";
			$count = $loop->post_count;
			$i = 1;

			while( $loop->have_posts() ): $loop->the_post();

				$link = get_permalink();
				$title = get_the_title();
				$date = DateTime::createFromFormat('Ymd', get_field('blog_date'));
				$summary = get_field('blog_summary');

				echo "<div class='date'>";
					echo $date->format('F j, Y');
				echo "</div>";

				echo "<div class='title'>";
					echo "$title";
				echo "</div>";

				echo "<div class='text'>";
					echo $summary;
					echo "<a class='link-indicator' href='$link'>read article</a>";
				echo "</div>";

				if ( $count != $i ) echo "<div class='hr'></div>";
				$i++;

			endwhile;

			echo "</div>"; //.blog-content


		}
		else {
			echo "No Blog posts? Go create some!";
		}
		wp_reset_postdata();
}



genesis();
