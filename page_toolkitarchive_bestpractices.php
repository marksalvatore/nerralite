<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: CPTookit Best Practices Archive 
 *
 */

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_cptoolkit_archive');

function nerra_display_cptoolkit_archive() {
	
	$doctype = 'best practice';
	echo "<div class='archive'>";
		echo "<div class='overview'>";
			echo "<h1>Collaborative project document archive</h1>";	
		echo "</div>";
		
		echo "<h1>Best practices</h1>";
		echo "<h2>Here’s a round up of best practices for planning, doing and wrapping up your collaborative project.</h2>";
		nerra_display_documenttypes($doctype);
	echo "</div>"; //.archive
}



genesis();