<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: About Friends
 *
 */

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_aboutfriends_page');
add_filter( 'body_class','nerra_addclass_aboutus' );
add_filter( 'body_class','nerra_addclass_whatweworkfor' );


function nerra_display_aboutfriends_page() {
	
	global $post;
	
	$custom_fields = get_post_custom(); // get all custom fields

	$friends_quote_image = $custom_fields['friends_quote_image'];
	$friends_quote_text = $custom_fields['friends_quote_text'];
	$friends_quote_authorname = $custom_fields['friends_quote_authorname'];
	$friends_quote_authorplace = $custom_fields['friends_quote_authorplace'];
	
	
	while ( have_posts() ) : the_post();
	
		echo "<section class='overview'>
					<header><h1>";
					
					the_title();
					
		echo "</h1></header><div class='text'>";
					
					the_content();
					
		echo "</div></header>";
		echo "</section>";
		
		echo "<div class='hr'></div>";

		// Quotes
		echo "<section class='quotes'>";		
	
				echo "<div class='image'>".wp_get_attachment_image($friends_quote_image[0], 'full')."</div>";
				echo "<div class='text'><q>".$friends_quote_text[0]."</q>";
					echo "<div class='author'>".$friends_quote_authorname[0]."</div><div class='place'>".$friends_quote_authorplace[0]."</div>";
				echo "</div>";

		echo "</section>";


	endwhile;
	
}


genesis();