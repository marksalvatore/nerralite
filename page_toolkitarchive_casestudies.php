<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: CPTookit Case Studies Archive
 *
 */

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_cptoolkit_archive');

function nerra_display_cptoolkit_archive() {
	
	$doctype = 'case study';
	echo "<div class='archive'>";
		echo "<div class='overview'>";
			echo "<h1>Collaborative project document archive</h1>";	
		echo "</div>";
		
		echo "<h1>Case studies</h1>";
		echo "<h2>Here’s a round up of case studies about planning, doing and wrapping up collaborative projects</h2>";
		nerra_display_documenttypes($doctype);
	echo "</div>"; //.archive
}


genesis();