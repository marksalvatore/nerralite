<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: HWW Training
 *
 */


remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_hwwtraining_overview');
add_action('genesis_loop', 'nerra_display_hwwtraining_issueblocks');
add_filter( 'body_class','nerra_addclass_howwework' );

function nerra_display_hwwtraining_overview() {
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1></header>
						<div class='text'>";
							the_content();
			echo "</div>";
		echo "</div>
					<div class='image'>";	
						the_post_thumbnail( 'full' );
		echo "</div>";
		echo "</section>";

	endwhile;
}
function nerra_display_hwwtraining_issueblocks() {
	
	$stories = array( 
				array ( 'page_id' => '704', 'title' => 'Advancing living shorelines along the Gulf Coast'),
				array ( 'page_id' => '711', 'title' => 'Controlling floods along New York’s Hudson River'),
				array ( 'page_id' => '713', 'title' => 'Protecting the Tiger Salamander in California'),
				array ( 'page_id' => '716', 'title' => 'Restoring oyster reefs in South Carolina'),
			);

	// array of pageids for the 4 projects you want to feature
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));
	$story_images = array ( 'HWW_Training_1_tn.png', 'HWW_Training_2_tn.png', 'HWW_Training_3_tn.png', 'HWW_Training_4_tn.png' );
	
	echo "<section class='blocks'>";
		echo "<div class='group'>";
		
			echo "<div class='one-fifth first'>";	
				echo "<div class='hottopic'>
								<div class='title'>
									Coastal Training
									<h4>featured projects</h4>
									<a class='link-indicator' href='index.php?page_id=719'>Browse</a>
								</div>
									<div class='image'>
										<img src='$path_to_resources/images/site/HWW_FeaturedProjects.png' alt='Featured projects'>
									</div>
								</div>";
			
			echo "</div><div class='four-fifths'>"; 
		
		$i=0;
		
		foreach ( $stories as $story ) {	
			
			if ( 0 == $i ) { 
					echo "<div class='one-fourth first'>"; 
			}	else {
					echo "<div class='one-fourth'>"; 				
			}
			echo "<div class='issue'>";
					echo "<a href='index.php?page_id=".$story['page_id']."'><div class='title'>".$story['title']."</div>";
					echo "<div class='image'><img src='$path_to_resources/images/site/$story_images[$i]' width='210' height='162' alt='".$story['title']."'></div></a>";
			echo "</div>";
			
			echo "</div>"; //.one-fourth
			$i++;
		}	
		echo "</div>"; //.four-fifths
		
	echo "</div></section>"; //.group .blocks

}


genesis();


