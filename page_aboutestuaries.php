<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: About Estuaries
 *
 */

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_aboutestuaries_page');
add_filter( 'body_class','nerra_addclass_aboutus' );


function nerra_display_aboutestuaries_page() {
	
	global $post;
	
	$custom_fields = get_post_custom(); // get all custom fields

	$estuaries_story_text = $custom_fields['estuaries_story_text'];
	$estuaries_story_image = $custom_fields['estuaries_story_image'];
	$estuaries_story_graph = $custom_fields['estuaries_story_graph'];
	
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1></header>
						<div class='text'>";
							the_content();
			echo "</div>";
		echo "</div>";
		echo "</section>";		

		echo "<div class='hr'>&nbsp;</div>";

		// Story
		echo "<section class='story'>";		
			echo "<div class='text'><p>".$estuaries_story_text[0]."</p></div>";			
			echo "<div class='image'>".wp_get_attachment_image($estuaries_story_image[0], 'full')."</div>"; // outside of story to be outside of margin	
		echo "</section>";

		echo "<section>";
				echo "<div class='graph'>".wp_get_attachment_image($estuaries_story_graph[0], 'full')."</div>";
		echo "</section>";


	endwhile;
	
}


genesis();