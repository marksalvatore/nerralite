<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Single Post Template: NERRA Strategies
* Description: Supports the detail page of a Collaborative Project Toolkit
*               tool, a matrix called Stakeholder Engagement Strategies. 
*/


remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_strategy');
add_filter( 'body_class','nerra_addclass_howwework' );
add_filter( 'body_class','nerra_addclass_page_toolkit' );

function nerra_display_strategy() {
	
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));	

	while ( have_posts() ) : the_post();

		$title = get_the_title();
		$overview = get_field('strategy_overview');
		$skills = get_field('strategy_skills');
		$tips = get_field('strategy_tips');
		$casestudy = get_field('strategy_case_study');
		$resources = get_field('strategy_resources');

		// Overview
		echo "<section class='overview'>
						<header>
							<h1>Doing</h1>
						</header>";
			echo "<div class='image'>";	
				echo "<a href='/how-we-work/collaborative-project-toolkit/'><img src='/wp-content/uploads/2015/06/Toolkit_ToolboxBig.png' alt='Collaborative Project Toolkit'></a>";	
			echo "</div>";
		echo "</section>";
	
		echo "<div class='strategy-detail'>";
		
			echo "<h2>$title</h2>";
			
			echo "<div class='hr'></div>";
			
			echo "<h3>Overview</h3>";
			echo "<div class='text'>";
		  	echo $overview;
			echo "</div>";
			echo "<div class='hr'></div>";
			
			echo "<h3>Skills & knowledge</h3>";
			echo "<div class='text'>";
		  	echo $skills;
			echo "</div>";
			echo "<div class='hr'></div>";
			
			echo "<h3>Tips for success</h3>";
			echo "<div class='text'>";
		  	echo $tips;
			echo "</div>";
			echo "<div class='hr'></div>";
			
			if ( ! empty($casestudy) ) {
				echo "<h3>Case study</h3>";
				echo "<div class='text'>";
			  	echo $casestudy;
				echo "</div>";
				echo "<div class='hr'></div>";
			}	
			if ( ! empty($resources) ) {
				echo "<h3>Resources</h3>";
				echo "<div class='text'>";
			  	echo $resources;
				echo "</div>";
			}

		echo "</div>"; // the new overview-text

	endwhile;
	
}




genesis();