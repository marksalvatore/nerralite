<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: CPToolkit Wrapping Up
 *
 */

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_hwwtoolkit');
add_filter( 'body_class','nerra_addclass_howwework' );
add_filter( 'body_class','nerra_addclass_page_toolkit' );

function nerra_display_hwwtoolkit() {
	
	while ( have_posts() ) : the_post();
	
		$title = get_the_title();
		
		// Overview
		echo "<section class='overview'>
						<header>
							<h1>Wrapping up</h1>
						</header>";
			echo "<div class='image'>";	
				echo "<a href='/how-we-work/collaborative-project-toolkit/'><img src='/wp-content/uploads/2015/06/Toolkit_ToolboxBig.png' alt='Collaborative Project Toolkit'></a>";	
			echo "</div>";
		echo "</section>";
	
		echo "<div class='overview-text'>";
		
		if ( ! is_page('wrapping-up') ) {
			echo "<h2>$title</h2>";
		}
						
		the_content();
					
		if ( ! is_page('wrapping-up') ) {
			// jump links
			nerra_display_toolkit_jumplinks();;
		}

		echo "</div>"; // overview-text
	
	endwhile;
	
	// Each subsection has a "termid" used to query 
	if ( is_page('share-your-work') )   {
		nerra_display_toolkit_shareyourwork();
	}
	if ( is_page('evaluate-your-project') )   {
		nerra_display_toolkit_evaluateyourproject();
	}
	if ( is_page('continue-to-collaborate') )   {
		nerra_display_toolkit_continuetocollaborate();
	}
	
}

function nerra_display_toolkit_shareyourwork(){
	
	$termid = 94; // category id for this subsection
	
	echo "<div class='hr'></div>";
	echo "<div class='toolkit-content'>";
	
		echo "<h3 id='bestpractices'>Best Practices</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'best practice'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
					
		echo "<div class='hr'></div>";
		
		echo "<h3 id='casestudies'>Case Studies</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'case study'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

		echo "<div class='hr'></div>";
		
		echo "<h3 id='resources'>Resources</h3>";
		echo "<div class='intro-text'>";
		echo "If you are interested in sharing your work with folks other than your stakeholders, 
					we encourage you to leverage the professional sharing networks and resources within 
					the National Reserve Estuarine Research Reserve System and the National Oceanic and Atmospheric Administration.";
		echo "</div>"; //.intro-text
					$doctype = 'resource'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

	echo "</div>"; // .toolkit-content	
	
}
function nerra_display_toolkit_evaluateyourproject(){
	
	$termid = 95; // category id for this subsection
	
	echo "<div class='hr'></div>";
	echo "<div class='toolkit-content'>";
	
		echo "<h3 id='bestpractices'>Best Practices</h3>";
		echo "<div class='intro-text'>";
		echo "Best practices in this section can help you implement a meaningful, 
					doable project evaluation plan and end your project by gathering lessons 
					learned and celebrating the hard work of the team.";
		echo "</div>"; //.intro-text
					$doctype = 'best practice'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
					
		echo "<div class='hr'></div>";

/*		
		echo "<h3 id='casestudies'>Case Studies</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'case study'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

		echo "<div class='hr'></div>";
*/		
		echo "<h3 id='resources'>Resources</h3>";
		echo "<div class='intro-text'>";
		echo "Tools and other resources in this section can help you define the purpose 
					and process for your project evaluation.";
		echo "</div>"; //.intro-text
					$doctype = 'resource'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

	echo "</div>"; // .toolkit-content	
	
}

function nerra_display_toolkit_continuetocollaborate(){
	
	$termid = 96; // category id for this subsection
	
	echo "<div class='hr'></div>";
	echo "<div class='toolkit-content'>";
	
		echo "<h3 id='bestpractices'>Best Practices</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'best practice'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
					
		echo "<div class='hr'></div>";
		
		echo "<h3 id='casestudies'>Case Studies</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'case study'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

/*		
		echo "<div class='hr'></div>";
		
		echo "<h3 id='resources'>Resources</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'resource'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
*/

	echo "</div>"; // .toolkit-content	
	
}

genesis();