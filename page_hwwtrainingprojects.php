<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: HWW Training Projects
 *
 */

// Referred to as the 'gallery page' for training

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_hwwtraining_overview');
add_action('genesis_loop', 'nerra_display_hwwtraining_issueblocks');
add_filter( 'body_class','nerra_addclass_howwework' );

function nerra_display_hwwtraining_overview() {
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
						<div class='group'>
							<header><h1>";
								the_title();
				echo "</h1></header>
							<div class='text'>";
								
				echo "</div>";
			echo "</div>
						<div class='image'>";	
							
			echo "</div>";
		echo "</section>";

	endwhile;
}
function nerra_display_hwwtraining_issueblocks() {
	
	$issues = array( 
				array ( 'page_id' => '704', 'title' => 'Advancing living shorelines along the Gulf Coast', 
								'description' => 'Shorelines that incorporate natural structures and systems can be more effective and affordable, have minimal negative impacts, and allow access for recreational use.'),				
				array ( 'page_id' => '711', 'title' => 'Controlling floods along New York’s Hudson River',
								'description' => 'The Hudson River reserve helped lead a community-driven process to assess risk and look for opportunities to adapt to climate change.'),
				array ( 'page_id' => '713', 'title' => 'Protecting the Tiger Salamander in California',
								'description' => 'This annual training has helped hundreds of professionals better understand how to conserve habitat for this endangered species.'),
				array ( 'page_id' => '716', 'title' => 'Restoring oyster reefs in South Carolina',
								'description' => '750 community volunteers helped construct two miles of oyster reef in the ACE Basin.'),

				array ( 'page_id' => '1294', 'title' => 'How to expect the unexpected in public meetings', 
								'description' => 'Public officials can address many of the frustrations driving people away from being engaged. They just need "a little motivation."'),		
				array ( 'page_id' => '1298', 'title' => 'Extra credit for North Carolina Realtors',
								'description' => 'Training helps realtors and property owners keep up with the latest in state coastal issues and management requirements.'),
				array ( 'page_id' => '1302', 'title' => 'The Hard and Soft of New Hampshire’s Shoreline Management',
								'description' => 'As sea levels rise and extreme storms become more frequent, the protection of coastal environments and properties has become a priority for the Granite State.'),
				array ( 'page_id' => '1306', 'title' => 'Preparing Washington’s coastal communities for change',
								'description' => 'Padilla Bay reserve training series helps local communities plan for climate change adaptation .'),
			);

	// array of pageids for the 8 issues you want to feature
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));
	$issue_images = array ( 'HWW_Training_1_tn.png', 'HWW_Training_2_tn.png', 'HWW_Training_3_tn.png', 'HWW_Training_4_tn.png', 'HWW_Training_5_tn.png', 'HWW_Training_6_tn.png', 'HWW_Training_7_tn.png', 'HWW_Training_8_tn.png' );
	
	echo "<section class='blocks'>";
		echo "<div class='group'>";
		
		$i = 0;


		foreach ( $issues as $issue ) {	
			
			if ( 0 == $i || 4 == $i ) { 
				echo "<div class='row'>";
					echo "<div class='one-fourth first'>"; 
			}	else {
					echo "<div class='one-fourth'>"; 				
			}
			
			echo "<div class='issue' id='".$issue['page_id']."'>";
			
				echo "<div class='issue-title'>";
					echo "<a href='index.php?page_id=".$issue['page_id']."'><div class='title'>".$issue['title']."</div>";
					echo "<img src='$path_to_resources/images/site/$issue_images[$i]' width='210' height='162' alt='".$issue['title']."'></a>";
				echo "</div>"; //.issue-title
			
				echo "<div class='issue-description'>";				
					echo "<a href='index.php?page_id=".$issue['page_id']."'>";
						echo "<div class='title'>".$issue['title']."</div>";
						echo "<div class='description'>".$issue['description']."</div>";								
					echo "</a>";
					echo "<a class='link-indicator'>Learn more</a>";	
				echo "</div>"; //.issue-descripton
			echo "</div>"; //.issue				
		echo "</div>"; //.one-fourth 
			
			if ( 3 == $i || 7 == $i ) { 
				echo "</div>"; //.row
			}
			$i++;
			
		}	

	echo "</div></section>"; //.group .blocks
}






genesis();


