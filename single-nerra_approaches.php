<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Single Post Template: NERRA Approach
* Description: Supports the detail page of a Collaborative Project Toolkit
*               tool, a matrix called Stakeholder Engagement Approaches
*/


remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_approach');
add_filter( 'body_class','nerra_addclass_howwework' );
add_filter( 'body_class','nerra_addclass_page_toolkit' );

function nerra_display_approach() {
	
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));	

	while ( have_posts() ) : the_post();

		$title = get_the_title();
		$overview = get_field('approach_overview');
		$groupsize = get_field('approach_group_size');
		$values = get_field('approach_participant_values');
		$time = get_field('approach_time_needed');
		$conditions = get_field('approach_conditions');
		$engagement = get_field('approach_stakeholder_engagement_level');
		$resources = get_field('approach_resources');

		// Overview
		echo "<section class='overview'>
						<header>
							<h1>Planning</h1>
						</header>";
			echo "<div class='image'>";	
				echo "<a href='/how-we-work/collaborative-project-toolkit/'><img src='/wp-content/uploads/2015/06/Toolkit_ToolboxBig.png' alt='Collaborative Project Toolkit'></a>";	
			echo "</div>";
		echo "</section>";
	
		echo "<div class='approach-detail'>";
		
			echo "<h2>$title</h2>";
			
			echo "<div class='hr'></div>";
			
			echo "<h3>Overview</h3>";
			echo "<div class='text'>";
		  	echo $overview;
			echo "</div>";
			echo "<div class='hr'></div>";
			
			echo "<h3>Group size</h3>";
			echo "<div class='text'>";
		  	echo $groupsize;
			echo "</div>";
			echo "<div class='hr'></div>";
			
			echo "<h3>Participant values, politics,<br />& culture</h3>";
			echo "<div class='text'>";
		  	echo $values;
			echo "</div>";
			echo "<div class='hr'></div>";
			
			echo "<h3>Time needed</h3>";
			echo "<div class='text'>";
		  	echo $time;
			echo "</div>";
			echo "<div class='hr'></div>";
			
			echo "<h3>Conditions</h3>";
			echo "<div class='text'>";
		  	echo $conditions;
			echo "</div>";
			echo "<div class='hr'></div>";
			
			echo "<h3>Stakeholder engagement level</h3>";
			echo "<div class='text'>";
		  	echo $engagement;
			echo "</div>";
			echo "<div class='hr'></div>";
			
			echo "<h3>Resources</h3>";
			echo "<div class='text'>";
		  	echo $resources;
			echo "</div>";

		echo "</div>"; // the new overview-text

	endwhile;
	
}




genesis();