<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Template Name: Tips on Speaking Up
* Description: A custom template
*/

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_speakup_page');


function nerra_display_speakup_page() {
	
	global $post;
	
	
	while ( have_posts() ) : the_post();

		$custom_fields = get_post_custom(); // get all custom fields
		
		$meetofficials_title = $custom_fields['speakup_meet_officials_title'];
		$meetofficials_text = $custom_fields['speakup_meet_officials_text'];
		$writeofficials_title = $custom_fields['speakup_write_officials_title'];
		$writeofficials_text = $custom_fields['speakup_write_officials_text'];
		$usesocial_title = $custom_fields['speakup_use_social_title'];
		$usesocial_text = $custom_fields['speakup_use_social_text'];
		$writeeditors_title = $custom_fields['speakup_write_editors_title'];
		$writeeditors_text = $custom_fields['speakup_write_editors_text'];
		$calltalkshows_title = $custom_fields['speakup_call_talk_shows_title'];
		$calltalkshows_text = $custom_fields['speakup_call_talk_shows_text'];

		
		$title = get_the_title();
		//$text = get_the_content();
		
		// Overview
		echo "<section class='overview'>
						<header><h1>$title</h1></header>
						<ul><li><a href='#meet'>Meet your elected officials face-to-face</a></li>
						<li><a href='#send'>Send a letter to your elected officials</a></li>
						<li><a href='#social'>Use social media</a></li>
						<li><a href='#write'>Write a letter to an editor</a></li>
						<li><a href='#call'>Call a talk show Up</a>
						</li></ul>";
		echo "</section>";

		echo "<div class='hr'></div>";

		
		// First
		echo "<section class='first-section' id='meet'>
						<div class='group'>
							<h2>".$meetofficials_title[0]."</h2>
							<div class='text'>".$meetofficials_text[0]."</div>";
			echo "</div>";
			echo "<div class='image'>";
						the_post_thumbnail( 'full' );
			echo "</div>";
		echo "</section>";
		
		echo "<div class='hr'></div>";
		
		
		// Second
		echo "<section class='second-section' id='send'>
						<div class='group'>
							<h2>".$writeofficials_title[0]."</h2>
							<div class='text'>".$writeofficials_text[0]."</div>";
			echo "</div>";			
		echo "</section>";

		echo "<div class='hr'></div>";
		
		// Default
		echo "<section class='default-section' id='social'>
						<div class='group'>
							<h2>".$usesocial_title[0]."</h2>
							<div class='text'>".$usesocial_text[0]."</div>
						</div>";		
		echo "</section>";

		echo "<div class='hr'></div>";
				
		// Default
		echo "<section class='default-section' id='write'>
						<div class='group'>
							<h2>".$writeeditors_title[0]."</h2>
							<div class='text'>".$writeeditors_text[0]."</div>
						</div>";		
		echo "</section>";

		echo "<div class='hr'></div>";
				
		// Speak Up
		echo "<section class='default-section' id='call'>
						<div class='group'>
							<h2>".$calltalkshows_title[0]."</h2>
							<div class='text'>".$calltalkshows_text[0]."</div>
						</div>";	
		echo "</section>";

			
	endwhile;

}



genesis();

