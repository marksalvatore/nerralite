<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template: Front Page
 *
 */

// Remove page title from display
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

// Setup shortcodes
add_shortcode( 'display_toolkit_icon', 'nerra_dsp_toolkit_icon' ); 	// referenced in Feature Story 4 widget

// Hook the HTML
// add_action( 'genesis_loop', 'nerra_display_featured_stories' );
add_action( 'genesis_loop', 'nerra_display_featured_story' ); // replacing featured_stories
add_action( 'genesis_loop', 'nerra_display_giving' );
add_action( 'genesis_loop', 'nerra_media_area' );

function nerra_display_featured_story() {
	$template_path = dirname(get_bloginfo('stylesheet_url'));
	echo "<div class='featured-story two-sixths'>";

	echo "<div class='title'>";
		echo "<a href='".get_site_url()."/my-reserve-our-coasts/'>";
		echo "<img src='$template_path/images/site/Home_MyReservesLogo.png' width='274' height='62' alt='My Reserve Our Coasts'></a>";
	echo "</div><!-- end .title -->";
	
	echo "<div class='text-big'>Save our</div>";
	echo "<div class='text'>NATIONAL RESERVE SYSTEM</div>";

	echo "<a href='".get_site_url()."/my-reserve-our-coasts/'>";
	echo "<button>ACT NOW</button></a>";

	echo "<div class='image'>";
		echo "<a href='".get_site_url()."/my-reserve-our-coasts/'>";
		echo "<img src='$template_path/images/site/Home_FeatureStory.jpg' width='271' height='435' alt='My Reserve Our Coasts'></a>";
		echo "</div><!-- end .image -->";

	echo "
	</div><!-- end .featured-stories -->
	</div><!-- end .featured-area -->";
}


function nerra_display_featured_stories($loop = 0) {

	$template_path = dirname(get_bloginfo('stylesheet_url'));

	echo "<div class='featured-stories two-sixths'>";

	$i = 1;
	$loop = nerra_get_featured_stories();

	if ( $loop ) {

		if( $loop->have_posts() ) {


			while( $loop->have_posts() ): $loop->the_post();
				$postid = get_the_ID();
				$title = get_the_title();
				$excerpt = get_the_excerpt();
				$image = get_field('story_image');
				//$file = get_field('story_file');
				$url = get_field('story_url');
				$projectlink = get_field('story_project_page');
				$storylink = get_field('story_project_story');

				// Without file in the picture, there are only two ways
				// to link a resource.
				$link = empty($projectlink) ? $storylink : $projectlink;
				$link = !empty($url) ? $url : $link;
				//echo "<li><a href=".$file['url'].">$title</a>
				$index = $i + 9;

				echo "<aside class='featured-story-0".$i."'>";
				echo "<section id='wdg_specialrecentpostsfree-$index' class='widget widget_specialrecentpostsFree'>";
					echo "<div class='srp-thumbnail-box'>";

						echo "<a class='srp-post-thumbnail-link' href='$link' title='$title'>
										<img src='".$image['url']."' class='srp-post-thumbnail' alt='$title' />
									</a>";
						echo "<div class='srp-content-box'>";
							echo "<h4 class='srp-post-title'>
											<a class='srp-post-title-link' href='$link' title='$title'>$title</a>
										</h4>";
							echo "<div class='srp-post-content'>$excerpt</div>";
							echo "</div>"; //.srp-content-box
						echo "</div>"; //.srp-thumbnail-box
					echo "</section>";
				echo "</aside>";
				$i++;

			endwhile;
		}

		// Toolkit image
		echo "<a href='".get_site_url()."/how-we-work/collaborative-project-toolkit/'><aside class='featured-story-04'>";
			echo "<img src='$template_path/images/site/HomePage_Toolkit_NoBox2b.png' width='349' height='183' alt='Collaborative Project Toolkit'>";
		echo "</aside></a>
					</div><!-- end .featured-stories -->
					</div><!-- end .featured-area -->";
	}
}


// Stories - display on home
function nerra_get_featured_stories( $term_id = 0) {
	/**
	 * Term ids (categories)
	 *
	 * categoryname1: 382
	 * categoryname2: 63
	 */

		$args = array(
			'post_type' => 'nerra_story',
			'orderby'   => 'meta_value_num',
			'meta_key'  => 'story_sort_order',
			'order'		=> 'asc',
			'posts_per_page' => 3, // limit to 3
			/*'tax_query' => array(
				array(
					'taxonomy' => 'nerra_story_placement',
					'field'    => 'term_id',
					'terms'    => $term_id,
				),
			),*/
		);

		$loop = new WP_Query( $args );

		return $loop;
}

function nerra_dsp_toolkit_icon() {
	//echo "<div class='icon-toolkit'><img src='$template_path/images/site/icon-toolkit.png' width='31' height='21' alt='NERRA Collaboration Research Guide'></div>";
}
function nerra_display_giving() {
	$template_path = dirname(get_bloginfo('stylesheet_url'));
	echo "<div class='giving-area'>
    			<div class='giving'>
    				<div class='image'>
    					<a href='give-today'><img  src='$template_path/images/site/HurricanRecovery_Graphic.png' width='287' height='96' alt='Give today' /></a>
    				</div>
					</div>
        </div>";

}
function nerra_media_area() {
	echo "<section class='media-area'>";

		echo "<div class='socialmedia one-third first'>";
			echo nerra_get_dsp_socialmedia();
		echo "</div><!-- .socialmedia -->";

		echo "<div class='socialnews one-third'>";
			echo nerra_display_socialnews();
		echo "</div><!-- .socialnews -->";

		echo "<div class='reserveslist one-third'>";
			echo nerra_display_reserveslist();
		echo "</div><!-- .reserveslist -->";

	echo "</section><!-- .media-area -->";
}

function nerra_get_dsp_socialmedia() {
	// Not sure why this won't pass variables, so must use this extra function call to send hardcoded values (fb_user, tw_user, tw_widget_id)
	// These values are only for the Social section on home, so they shouldn't have to change.
	// parameters ($facebook, $twitter, $twitterID, $instagram) estuary99
	return nerra_display_socialmedia_boxes( 'estuaries', 'Estuaries4Life','estuaries4life', '635562693081083909' );
}
function nerra_display_socialnews() {

		$args = array(
			'meta_key'		=> 'news_date', // use meta_key for custom fields
			'orderby'			=> 'meta_value',
			'order'				=> 'desc',
			'posts_per_page'=> '7', // overrides posts per page in theme settings
			'post_type' 	=> 'nerra_news', // here's the magic
		);
		$loop = new WP_Query( $args );

		echo "<h3><a href='".get_site_url()."/nerra-news-page/'>Blog</a></h3>";

		if( $loop->have_posts() ) {

			while( $loop->have_posts() ): $loop->the_post();

				$link = get_permalink();
				$title = get_the_title();
				$date = DateTime::createFromFormat('Ymd', get_field('news_date'));
				$summary = get_field('news_summary');
				$story = get_field('news_story');

				echo "<div class='newsitem'>";
					echo "<div class='news-date'>".$date->format('F j, Y')."</div>";
					echo "<div class='news-title'><a class='internal-link' href='$link'>$title</a></div>";
					echo "<div class='news-summary'>$summary</div>";
				echo "</div><!-- .newsitem -->";

			endwhile;

		}
		else {
			echo "No News items to report.";
		}

		//wp_reset_postdata();
}
function nerra_display_reserveslist() {
	$template_path = dirname(get_bloginfo('stylesheet_url'));
	echo "<div class='image'>
					29 reserves nationwide
					<!--
					<img src='$template_path/images/site/reserves_IN_states.png' width='275' alt='There are many reserves in several states in the USA'>
					23 states
					-->
				</div>";

	//reserves list here
	$args = array(
		//'category_name' => 'nerra-reserve', // Not needed, since using all records in cpt
		'orderby'       => 'title',
		'order'         => 'asc',
		'posts_per_page'=> '30',
		'post_type' 	=> 'nerra_reserve', // here's the magic
	);
	$loop = new WP_Query( $args );

	if( $loop->have_posts() ) {

		//echo "<div class='hr'></div>";

		$i = 0;

		while( $loop->have_posts() ): $loop->the_post();

			$i++;
			if ( 1 == $i ) {
				echo "<div class='one-half first'>";
			}
			if ( 15 == $i ) {
				echo "<div class='one-half'>";
			}
			$title = get_field('reserve_shortname');
			$state = strtoupper(get_field('reserve_vitals_state_code'));
			$url = get_field('reserve_url');

			echo '<a href="' . $url . '" target="_blank">' . $title . ' ('.$state.')</a>';

			if ( 14 == $i ) {
				echo "</div>"; // end .one-fifth
			}

		endwhile;

		echo "</div><!-- end second column in list -->";

	}
	//wp_reset_postdata();

}

genesis();
