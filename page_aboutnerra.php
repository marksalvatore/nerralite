<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: About NERRA
 *
 */

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_aboutnerra_page');
add_filter( 'body_class','nerra_addclass_aboutus' );


function nerra_display_aboutnerra_page() {
	
	global $post;
	
	$custom_fields = get_post_custom(); // get all custom fields

	$nerra_leadership_text = $custom_fields['nerra_leadership_text'];
	$nerra_funding_text = $custom_fields['nerra_funding_text'];
	$estuaries_day_text = $custom_fields['estuaries_day_text'];
	$estuaries_day_image = $custom_fields['estuaries_day_image'];
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>";
			
			echo "<ul class='jump-menu'><li><a href='#leadership'>Leadership</a></li><li><a href='#funding'>Funding</a></li><li><a href='#estuariesday'>Estuaries Week</a></li><li><a href='#contact'>Contact</a></li></ul>";	
			echo "<div class='group'>";
				echo "<header><h1>";	
					the_title();		
				echo "</h1></header>";
				echo "<div class='text'>";
					the_content();				
				echo "</div>";
			echo "</div>";
			
		echo "</section>";
	
		echo "<div class='hr'></div>";

		// Leadership
		echo "<section class='leadership' id='leadership'>";
		echo "<h2>Leadership</h2>";
		echo "<div class='text'>".$nerra_leadership_text[0]."</div>";
		
		echo "<div class='tabs staff-tab current-tab'>Staff</div>";
		echo "<div class='tabs executive-tab'>Executive Committee</div>";
		echo "<div class='tabs governing-tab'>Governing Board</div>";
		
		echo "<div class='leadership-area'>";	
		
			// Staff
			$loop = nerra_get_people_by_termid(67); // staff leadership category (term id)	
			nerra_display_staff($loop);

			// Executive Committee	
			$loop = nerra_get_people_by_termid(68, 'people_last_name'); // exec leadership category (term id)		
			nerra_display_executive($loop);
			
			// Governing Board
			$loop = nerra_get_people_by_termid(69, 'people_last_name'); // gov leadership category (term id)		
			nerra_display_governing($loop);
		
		echo "</div>"; //.leadership-area
		echo "</section>"; //.leadership
		
		// Funding Reserves & NERRA
		echo "<div class='funding' id='funding'>";
		echo "<h2>Funding for Reserves and NERRA</h2>";
		echo "<div class='text'>".$nerra_funding_text[0]."</div>";	
		//echo "<h3>Fiscal year appropriations request:</h3>";	
				
			/* Removed following two lines March 3, 2016
  			in place of using the admin form field to add a list of pdf links.
  			From Dashboard: PAGE - How NERRA supports the reserves >> Nerra Funding Text
  			
    		$loop = nerra_get_documents_by_termid(82); // funding category (or term id)
  			nerra_display_documents($loop);
		*/
		echo "</div>";
		
		echo "<div class='hr'></div>";

		// National Estuaries Week
		echo "<div class='estuariesday' id='estuariesday'>";
			echo "<h2>National Estuaries Week</h2>";
			echo "<div class='group'>";
				echo "<div class='text'>".$estuaries_day_text[0]."</div>";	
				echo "<div class='image'>".wp_get_attachment_image($estuaries_day_image[0], 'full')."</div>"; 
			echo "</div>";
		echo "</div>";
		
	endwhile;
	
}

// Leadership
function nerra_get_people_by_termid( $term_id = 0, $orderby = 'post_id') {
	
	/**
	 * Term ids
	 * 
	 * Staff: 67
	 * Executive: 68 
	 * Governing: 69 
	 */

	//$orderby = ('post_id' == $meta_key) ? 'meta_value_num' : 'meta_value';

	if ('post_id' == $orderby ) {
		$args = array( // basic, because using post_id
			'post_type' => 'nerra_people',
			'posts_per_page' => -1, // to assure all posts are returned
			'orderby'    => 'post_id',
			'order'		=> 'asc',
			'tax_query' => array(
				array(
					'taxonomy' => 'nerra_people_categories',
					'field'    => 'term_id',
					'terms'    => $term_id,
				),
			),
		);
	} else {
		$args = array( // custom, because using 'people_last_name', a custom field to search
			'post_type' => 'nerra_people',
			'posts_per_page' => -1, // to assure all posts are returned
			'meta_key'			=> $orderby, // use meta_key for custom fields
			'orderby'			=> 'meta_value',
			'order'		=> 'asc',
			'tax_query' => array(
				array(
					'taxonomy' => 'nerra_people_categories',
					'field'    => 'term_id',
					'terms'    => $term_id,
				),
			),
		);		
	}
	
	$loop = new WP_Query( $args );
	
	return $loop;
}
function nerra_display_staff( $loop = 0) {

	echo "<section class='staff'>";
	
	
	if ( $loop ) {
	
		if( $loop->have_posts() ) {
			while( $loop->have_posts() ): $loop->the_post();
			
				$postid = get_the_ID();
				$fullname = get_the_title();
				$firstname = get_field('people_first_name');
				$title = get_field('people_title');
				$photo = get_field('people_photo');
				$email = get_field('people_email');
				
				if ( 494 == $postid ) {
						echo "<div class='current-card' id='$postid'>";					
				} else {
						echo "<div class='card' id='$postid'>";					
				}
					if ( ! empty ( $photo ) ) {
						echo "<div class='photo'><img src=".$photo['url']." /></div>";
					} else {
						echo "<div class='photo'><img src='http://placehold.it/152x149'></div>";
					}
					echo "<div class='group'>";
						echo "<div class='name'>$fullname</div>";
						echo "<div class='title'>$title</div>";
					echo "</div>";
				echo "</div><!-- //.card -->";
				
			endwhile;	
			
			
			wp_reset_postdata(); // rewind the same set of posts to output the biographies
			
			echo "<div class='biographies'>";

			while( $loop->have_posts() ): $loop->the_post();

				$postid = get_the_ID();
				$fullname = get_the_title();
				$firstname = get_field('people_first_name');
				$title = get_field('people_title');
				$email = get_field('people_email');
				$bio = get_the_content();
				
				
				if ( 494 == $postid ) { // display Rebecca Roth as default
						echo "<div class='biography-default'>";
							echo "<div class='name'>$fullname</div>";
							echo "<div class='title'>$title</div>";
							if ( ! empty( $email ) ) {
								echo "<a class='link-indicator' href='mailto:$email'>Contact $firstname</a>";
							}
							echo "<div class='text'><p>$bio</p>";
						echo "</div>"; //.text
						echo "</div><!-- //.biography-defalut -->";					
				} 						
				echo "<div class='biography-$postid'>";
					echo "<div class='name'>$fullname</div>";
					echo "<div class='title'>$title</div>";
							if ( ! empty( $email ) ) {
								echo "<a class='link-indicator' href='mailto:$email'>Contact $firstname</a>";
							}
					echo "<div class='text'><p>$bio</p>";
					echo "</div>"; //.text
				echo "</div><!-- //.biography -->";
				
			endwhile;
	
			echo "</div>"; //.biographies

		}		

	}
	echo "</section><!-- //.staff -->";
}
function nerra_display_executive( $loop = 0) {

	echo "<section class='executive'>";
	
	if ( $loop ) {
	
		if( $loop->have_posts() ) {
			
			$i=0;
			
			while( $loop->have_posts() ): $loop->the_post();
			
				$i++;
			
				$postid = get_the_ID();
				$fullname = get_the_title();
				$firstname = get_field('people_first_name');
				$title = get_field('people_title');
				$photo = get_field('people_photo');
				$email = get_field('people_email');
				

				// Determine current card
				if ( 532 == $postid ) { // display William Reay as default
						echo "<div class='current-card' id='$postid'>";					
				} else {
						echo "<div class='card' id='$postid'>";					
				}
				
				if ( ! empty ( $photo ) ) {
					echo "<div class='photo'><img src=".$photo['url']." /></div>";
				} else {
					echo "<div class='photo'><img src='http://placehold.it/152x149'></div>";
				}
				
				echo "<div class='group'>";
					echo "<div class='name'>$fullname</div>";
					echo "<div class='title'>$title</div>";
				echo "</div>";
				
				echo "</div>"; //.card

			endwhile;	
			
			
			wp_reset_postdata(); // rewind the same set of posts to output the biographies
			
			echo "<div class='biographies'>";

			while( $loop->have_posts() ): $loop->the_post();

				$postid = get_the_ID();
				$fullname = get_the_title();
				$firstname = get_field('people_first_name');
				$title = get_field('people_title');
				$email = get_field('people_email');
				$bio = get_the_content();
				
				
				if ( 532 == $postid ) { // display William Reay as default
						echo "<div class='biography-default'>";
							echo "<div class='name'>$fullname</div>";
							echo "<div class='title'>$title</div>";
							if ( ! empty( $email ) ) {
								echo "<a class='link-indicator' href='mailto:$email'>Contact $firstname</a>";
							}
							echo "<div class='text'><p>$bio</p>";
						echo "</div>"; //.text
						echo "</div><!-- //.biography-defalut -->";					
				} 						
				echo "<div class='biography-$postid'>";
					echo "<div class='name'>$fullname</div>";
					echo "<div class='title'>$title</div>";
							if ( ! empty( $email ) ) {
								echo "<a class='link-indicator' href='mailto:$email'>Contact $firstname</a>";
							}
					echo "<div class='text'><p>$bio</p>";
					echo "</div>"; //.text
				echo "</div><!-- //.biography -->";
				
			endwhile;
	
			echo "</div>"; //.biographies

		}		

	}
	echo "</section><!-- //.executive -->";
}
function nerra_display_governing( $loop = 0) {

	echo "<section class='governing'>";

	if ( $loop ) {

		if( $loop->have_posts() ) {
			
			$i=0;
			
			while( $loop->have_posts() ): $loop->the_post();
			
				$i++;
			
				$fullname = get_the_title();
				$title = get_field('people_title');
				$email = get_field('people_email');

				// Determine column
				if ( 1 == $i ) {		
						echo "<div class='one-fifth first'>";
				} else if ( 10 == $i || 19 == $i || 28 == $i ) {
						echo "<div class='one-fifth'>";
				}
				
				echo "<div class='entry'>";
				
					echo "<div class='name'>$fullname</div>";
					/* Display reserve name */
					$reserve_obj = get_field('people_associated_reserve');
					
					if( $reserve_obj ) {			
						echo "<div class='reserve'>";
							setup_postdata( $reserve_obj ); 
							
							// HACK:: to replace reserve association with other entity
							if ( preg_match("/charov/i", $fullname ) ) {
								// do nothing
							} elseif (preg_match("/asuncion/i", $fullname) ) {
								echo 'Hawaii (in process)';
							} else {
								echo $reserve_obj->reserve_shortname;
							}

						echo "</div>";
					}
					echo "<div class='title'>$title</div>";
					echo "<a class='link-indicator' href='mailto:$email'>Contact</a>";
					
				echo "</div><!-- //.entry -->";
				
				if ( 9 == $i || 18 == $i || 27 == $i || 36 == $i ) {		
					echo "</div>"; //.one-fifth
				}

			endwhile;	
			
			if ( $i < 36 ) { // in case the column doesn't get closed
				echo "</div>";
			}
		}		
	}
	echo "</section><!-- //.executive -->";

}




genesis();