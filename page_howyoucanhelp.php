<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: How You Can Help 
 *
 */


remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_howyoucanhelp_page');
add_filter( 'body_class','nerra_addclass_page_primary' );


function nerra_display_howyoucanhelp_page() {
	
	global $post;
	
	while ( have_posts() ) : the_post();

		$custom_fields = get_post_custom(); // get all custom fields
		
		$visit_text = $custom_fields['help_visit_text'];
		$visit_image = $custom_fields['help_visit_image'];
		$give_text = $custom_fields['help_give_text'];
		$give_image = $custom_fields['help_give_image'];
		$volunteer_text = $custom_fields['help_volunteer_text'];
		$speakup_text = $custom_fields['help_speakup_text'];
		$speakup_image = $custom_fields['help_speakup_image'];
		
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1></header>
						<div class='text'>";
				echo "<ul><li><a href='#visit'>Visit</a></li>
							<li><a href='#give'>Give</a></li>
							<li><a href='#volunteer'>Volunteer</a></li>
							<li><a href='#speakup'>Speak Up</a>
							</li></ul>";
							the_content();
			echo "</div>";
		echo "</div>
					<div class='image'>";	
						the_post_thumbnail( 'full' );
		echo "</div>";
		echo "</section>";

		
		// Visit
		echo "<section class='visit' id='visit'>
						<div class='image'>".wp_get_attachment_image($visit_image[0], 'full')."</div>
						<div class='group'>
							<h2>Visit</h2>
							<div class='text'>".$visit_text[0]."</div>
						</div>";					
		echo "</section>";
		
		// Give
		echo "<section class='give' id='give'>
						<div class='image'><a href='give-today/'>".wp_get_attachment_image($give_image[0], 'full')."</a></div>";
			echo "<div class='group'>
							<h2>Give</h2>
							<div class='text'>".$give_text[0]."</div>
						</div>					
						<div class='hr'></div>";
		echo "</section>";
		
		// Volunteer
		echo "<section class='volunteer' id='volunteer'>
						<h2>Volunteer</h2>
						<div class='text'>".$volunteer_text[0]."</div>";				
		echo "</section>";

		echo "<div class='hr'></div>";
				
		// Speak Up
		echo "<section class='speakup' id='speakup'>
						<div class='group'>
							<h2>Speak up</h2>
							<div class='text'>".$speakup_text[0]."</div>
						</div>
						<div class='image'>".wp_get_attachment_image($speakup_image[0], 'full')."</div>";			
		echo "</section>";
		
	endwhile;

}



genesis();

