<?php
/**
 * The template for displaying archive pages
 *
 */
?>

<?php get_header(); ?>

<section class='overview'>
	<div class='group'>
		<header>
			<?php 
			$path_to_resources = dirname(get_bloginfo('stylesheet_url'));
			if ( have_posts() ) : 
			    single_term_title( '<h1>', '</h1>' );
			endif; 
			?>
		</header>
	</div>
	<div class='image'>
		
	</div>
</section>

<section class="news-page">

 	<div class="news-tags">
 		<h2>Categories</h2>
 		<?php echo "<a class='all-posts' href='".get_site_url()."/nerra-news-page/'>ALL BLOG POSTS</a>"; ?>
		<?php  wp_list_categories('title_li='); ?>
	</div>

	<?php if( have_posts() ) :

		while ( have_posts() ) : the_post();

			$link = get_permalink();
			$title = get_the_title();
			$date = DateTime::createFromFormat('Ymd', get_field('news_date'));
			$summary = get_field('news_summary');

			echo "<div class='news-content'>";

			echo "<div class='date'>";
				echo $date->format('F j, Y');
			echo "</div>";
			
			echo "<div class='title'>";
				echo "<a href='$link'>$title</a>";
			echo "</div>";
			
			echo "<div class='text'>";
				echo $summary;
				echo "<a class='link-indicator' href='$link'>read more</a>";
			echo "</div>";

			echo "<div class='hr'></div>";

		endwhile;

		echo "</div>"; //.news-content

	endif; ?>

</section>

<?php get_footer();
