<?php
/**
 * Plugin Name: NERRA Content Types
 * Description: Add custom post types and taxonomies for NERRA
 * Version: 1.0
 * Author: Mark Salvatore
 * License: GNU General Public License v3.0
 * License URI: http://www.opensource.org/licenses/gpl-license.php
 */

	/* MS:: reserved words:
	  
	    post
	    page
	    attachment
	    revision
	    nav_menu_item
	    action
	    order
	    theme
	    
	    You can customize message output, context help, and help tabs for your post type.
	*/
	
add_action( 'rightnow_end', 'nerra_content_types', 9 );

function nerra_content_types()
{
  // Content type "nerra_stories"
  $labels = array(
    'name'                => 'NERRA Stories',
    'singular_name'       => 'NERRA Story',
    'menu_name'           => 'NERRA Stories',
    'name_admin_bar'      => 'NERRA Story',
    'add_new'             => 'Add New',
    'add_new_item'        => 'Add New NERRA Story',
    'new_item'            => 'New NERRA Story',
    'edit_item'           => 'Edit NERRA Story',
    'view_item'           => 'View NERRA Story',
    'all_items'           => 'All NERRA Stories',
    'search_items'        => 'Search NERRA Stories',
    'parent_item_colon'   => 'Parent NERRA Stories:',
    'not_found'           => 'No NERRA Stories found.',
    'not_found_in_trash'  => 'No NERRA Stories found in Trash.'
  );
  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-id-alt',
    'query_var'           => true,
    'rewrite'             => array('slug' => 'nerra-stories'),
    
     // These 3 settings determine post/page behavior ('page', false, true)
    'capability_type'     => 'post',
    'has_archive'         => true,
    'hierachical'         => false,
    
     // 'supports' is an alias for add_post_type_support()
     // Note: support custom_fields in order to create fields not included with WP
    'supports'            => array('title','editor', 'custom-fields', 'thumbnail'),
    
    'taxonomies' => array('category', 'post_tag')
  );
  register_post_type('stories', $args);
  
  // END content type "nerra_stories"
  
  /*
  // Content type "nerra_impacts"
  $labels = array(
    'name'                => 'Project Impacts',
    'singular_name'       => 'Project Impact',
    'menu_name'           => 'Project Impacts',
    'name_admin_bar'      => 'Project Impact',
    'add_new'             => 'Add New',
    'add_new_item'        => 'Add New Project Impact',
    'new_item'            => 'New Project Impact',
    'edit_item'           => 'Edit Project Impact',
    'view_item'           => 'View Project Impact',
    'all_items'           => 'All Project Impacts',
    'search_items'        => 'Search Project Impacts',
    'parent_item_colon'   => 'Parent Project Impacts:',
    'not_found'           => 'No Project Impacts found.',
    'not_found_in_trash'  => 'No Project Impacts found in Trash.'
  );
  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 6,
    'menu_icon'           => 'dashicons-format-image',
    'query_var'           => true,
    'rewrite'             => array('slug' => 'project-impacts'),
    
     // These 3 settings determine post/page behavior ('page', false, true)
    'capability_type'     => 'post',
    'has_archive'         => true,
    'hierachical'         => false,
    
     // 'supports' is an alias for add_post_type_support()
    'supports'            => array('title','editor', 'custom-fields', 'thumbnail'),
    
    'taxonomies' => array('category', 'post_tag')
  );
  register_post_type('nerra_impacts', $args);
  // END content type "nerra_Impacts"
 */
  
}

/* NOT USED FOR plugins/plugins
	
add_action('init', 'nerra_content_types');

function myplugin_flush_rewrites() {
	nerra_content_types(); // call your CPT registration function
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'myplugin_flush_rewrites' );

function myplugin_flush_rewrites_deactivate() {
	flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'myplugin_flush_rewrites_deactivate' );

*/