<?php if ( ! defined( 'ABSPATH' ) ) exit( 'Direct access not permitted.' );

/**
 * Setup
 *
 * This file registers widgets and theme support, loads scripts, adds and removes genesis actions
 *
 */

//* Add viewport meta tag for mobile browsers
//add_theme_support( 'genesis-responsive-viewport' );
add_action( 'genesis_setup', 'nerra_setup_theme' );
add_action('wp_head', 'insert_viewport_device_width');
function insert_viewport_device_width(){
	//echo "<meta name='viewport' content='width=device-width, initial-scale=1'>";
	echo "<meta name='viewport' content='1200, initial-scale=1'>";
	//echo "<meta name='viewport' content='width=1200'>";
	//echo "<meta name='viewport'>";
}



function nerra_setup_theme() {

	// * REMOVE STUFF * //

	// Remove unnecessary title
	remove_action( 'genesis_site_title', 'genesis_seo_site_title' );

	//* Remove the primary and secondary sidebar
	unregister_sidebar( 'sidebar' );
	unregister_sidebar( 'sidebar-alt' );
	remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
	remove_action( 'genesis_sidebar_alt', 'genesis_do_sidebar_alt' );

	//* Remove site layouts
	genesis_unregister_layout( 'content-sidebar' );
	genesis_unregister_layout( 'sidebar-content' );
	genesis_unregister_layout( 'content-sidebar-sidebar' );
	genesis_unregister_layout( 'sidebar-sidebar-content' );
	genesis_unregister_layout( 'sidebar-content-sidebar' );

	//* Move primary  and sub navs into the header
	add_action('genesis_before_header', 'nerra_google_analytics');
	remove_action( 'genesis_after_header', 'genesis_do_nav' );
	add_action( 'genesis_header', 'genesis_do_nav' );
	remove_action( 'genesis_after_header', 'genesis_do_subnav' );
	add_action('genesis_header', 'nerra_genesis_do_subnav');

	// Display hook location (development utility)
	//add_action( 'genesis_before_content', 'show_hook_location' );



	// * CHANGE STUFF * //

	// Set current state of the orange, secondary nav based on primary nav
	add_filter( 'wp_nav_menu_args', 'nerra_select_secondary_menu' );

	// Replace Genesis footer with our own
	remove_action( 'genesis_footer', 'genesis_do_footer' );
	add_action( 'genesis_footer', 'nerra_display_footer' );


	// * ADD STUFF * //

	add_filter( 'genesis_edit_post_link', '__return_false' ); // gets rid of the EDIT link... ugh!
	add_action( 'widgets_init', 'nerra_widget_deregistration' ); // get rid of genesis default inits
	add_action( 'genesis_loop', 'nerra_sidebar_html' ); // most homepage-specific areas have a widget

	add_theme_support( 'html5' );
	add_action( 'wp_enqueue_scripts', 'nerra_load_styles' ); // css
	add_action( 'wp_enqueue_scripts', 'nerra_load_scripts' ); // javascript
	add_action('genesis_header', 'nerra_dsp_bordertop', 1);
	add_action('genesis_site_description', 'nerra_dsp_byline');


	// NEWS and CATEGORIES
	// By default, WordPress will only include the "post" post type 
	// within the post_tag and category taxonomy archives. Use this
	// filter to have nerra_news work with categories/tags
	add_filter( 'pre_get_posts', 'nerra_news_taxonomy_archives' );
	function nerra_news_taxonomy_archives( $query ) {
	  if ( $query->is_main_query() && ( is_category() || is_tag() ) ) {
	        $query->set( 'post_type', array( 'post', 'nerra_news' ) );
	  }
	}

	// Display CATEGORIES for nerra_news posts
	function get_cats(){
	$categories = get_the_category();
	$separator = ' :: ';
	$output = '';
	if ( ! empty( $categories ) ) {
	    foreach( $categories as $category ) {
	        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
	    }
	    echo trim( $output, $separator );
	}}


/* ************* SEARCH ****************
//* Customize search form input box text
add_filter( 'genesis_search_text', 'sp_search_text' );
function sp_search_text( $text ) {
	return esc_attr( 'Search my blog...' );
}



//* Customize search form input button text
add_filter( 'genesis_search_button_text', 'sp_search_button_text' );
function sp_search_button_text( $text ) {
	return esc_attr( 'Go' );
}


//* Customize search form label
add_filter( 'genesis_search_form_label', 'sp_search_form_label' );
function sp_search_form_label ( $text ) {
	return esc_attr( 'Custom Label' );
}

*/






	nerra_create_all_custom_taxonomies();

	// Register all sidebars
	nerra_sidebar_declaration_registration();

	// Add doctype filters to media library (pdf, doc, docx)
	add_filter( 'post_mime_types', 'nerra_modify_post_mime_types' );



}

// Head
function nerra_load_scripts() {
  wp_register_script( 'nerra', get_stylesheet_directory_uri() . '/js/nerra-min.js', array( 'jquery' ), '', true );
  wp_enqueue_script('nerra');
	if ( is_page('collaborative-research') || is_page('find-your-reserve') ) {
  	wp_register_script('mapbox', '//api.mapbox.com/mapbox.js/v2.2.1/mapbox.js');
  	wp_enqueue_script('mapbox');
	}
	if ( is_page('collaborative-research') ) {
  	wp_register_script( 'mapbox-projects', get_stylesheet_directory_uri() . '/js/mapbox-projects-min.js', array( 'jquery' ), '', true );
  	wp_enqueue_script('mapbox-projects');
	}
	if ( is_page('find-your-reserve') ) {
  	wp_register_script( 'mapbox-reserves', get_stylesheet_directory_uri() . '/js/mapbox-reserves-min.js', array( 'jquery' ), '', true );
  	wp_enqueue_script('mapbox-reserves');
	}
}


function nerra_load_styles() {
  wp_enqueue_style( 'dashicons' );
  wp_enqueue_style('mapboxcss', '//api.mapbox.com/mapbox.js/v2.2.1/mapbox.css');
}

function nerra_widget_deregistration() {

	unregister_widget( 'Genesis_eNews_Updates' );
	unregister_widget( 'Genesis_Latest_Tweets_Widget' );
	unregister_widget( 'Genesis_Featured_Page' );
	unregister_widget( 'Genesis_Featured_Post' );
	//unregister_widget( 'Genesis_Widget_Menu_Categories' );
	//unregister_widget( 'Genesis_Menu_Pages_Widget' );
	unregister_widget( 'Genesis_User_Profile_Widget' );

	unregister_widget( 'Akismet_Widget' );

	//unregister_widget( 'WP_Widget_Pages' );
	unregister_widget( 'WP_Widget_Calendar' );
	//unregister_widget( 'WP_Widget_Archives' );
	//unregister_widget( 'WP_Widget_Links' );
	//unregister_widget( 'WP_Widget_Meta' );
	unregister_widget( 'WP_Widget_Search' );
	//unregister_widget( 'WP_Widget_Text' );
	//unregister_widget( 'WP_Widget_Categories' );
	unregister_widget( 'WP_Widget_Recent_Posts' );
	unregister_widget( 'WP_Widget_Recent_Comments' );
	unregister_widget( 'WP_Widget_RSS' );
	unregister_widget( 'WP_Widget_Tag_Cloud' );
	//unregister_widget( 'WP_Nav_Menu_Widget' );
}

// Header
function nerra_dsp_bordertop() {
	echo '<div class="site-bordertop"></div><!-- end .site-bordertop -->';
}
function nerra_dsp_byline() {
	if (is_front_page()) { // display on home only
		echo '<aside class="org-byline">';
	}
	else { // hide it on other pages
		echo '<aside class="org-byline hidden">';
	}
	echo 'We support <br/>coasts & estuaries
			  </aside><!-- end .org-byline -->';
}
function nerra_genesis_do_subnav() {
	// DO NOT DISPLAY SUBNAV (orange bar) on these pages:
	if( ! is_front_page() and
			! is_page('find-your-reserve') and
			! is_page('nerra-reserve') and
			! is_page('how-you-can-help') and
			! is_page('give-today') and
			! is_page('privacy-statement') and
			! is_page('nerra-news-page') and
			! is_page('search') and
			! is_search() and
			! is_404() and
			! is_page('collaborative-project-toolkit-best-practices-archive') and
			! is_page('collaborative-project-toolkit-case-studies-archive') and
			! is_page('tips-on-speaking-up-for-the-reserves') and  // speakup
			! is_category('') and // any category
			! is_singular( array( 'nerra_reserve' ) ) and
			! is_singular( array( 'nerra_news' ) ) and
			! is_page( array( 'my-reserve-our-coasts' ) )   ) {
		genesis_do_subnav();
	}
}

// Body - sidebars must be set during initialization
function nerra_sidebar_declaration_registration() {
	$sidebars = array(
		array(
			'id'					=> 'reserve-slider',
			'name'				=> __( 'Reserve Slider', CHILD_DOMAIN ),
			'description'	=> __( 'NERRA Reserve slides on home.', CHILD_DOMAIN ),
		),
			array(
			'id'					=> 'toolkit-menu',
			'name'				=> __( 'Toolkit Menu', CHILD_DOMAIN ),
			'description'	=> __( 'Displays on Collaborative Project Toolkit pages.', CHILD_DOMAIN ),
		)
	);

	foreach ( $sidebars as $sidebar )
		genesis_register_sidebar( $sidebar );
}
function nerra_sidebar_html() {
		genesis_widget_area(
    'reserve-slider',
    array( 'before' => '<div class="featured-area"><div class="reserve-slider"><aside class="reserve-slider-caption one-sixth first">&nbsp;</aside><aside class="reserve-slider-image three-sixths">',
    			 'after'  => '</aside><!-- end .reserve-slider-image -->
    			 							</div><!-- end .reserve-slider -->',
    )
	);
	genesis_widget_area(
    'toolkit-menu',
    array( 'before' => '<div class="toolkit-menu">
    										<h3><a href="/how-we-work/collaborative-project-toolkit/">Inside this toolkit</a></h3>',
           'after'  => '</div><!-- end .toolkit-menu -->',
    )
	);
}
function nerra_display_socialmedia_boxes( $facebook_username = '', $facebook_page = '', $tw_user = '', $tw_widgetid = '' ) {

		//  - add youtube and link panels
		// see http://answers.squarespace.com/questions/68076/how-to-embed-a-youtube-channel
		// put instagram on the links tab, don't give it its own unless it can be embedded


	echo  "<h3>Social</h3>
				<ul class='tabs'>
					<li class='facebook-tab'><span href='#' class='dashicons dashicons-facebook'></span></li>";
					if ( strlen($tw_user) && strlen($tw_widgetid) ) {
						echo "<li class='twitter-tab'><span href='#' class='dashicons dashicons-twitter'></span></li>";

					}
					/*
					if ( strlen($instagram_user) ) {
						echo "<li class='instagram-tab'><span href='#' class='dashicons dashicons-camera'></span></li>";
					}*/


				echo "</ul>";


	// FACEBOOK - Determine if $facebook_username is a feed to an account or a page
	echo "<div class='facebook-content'>";

	if ( $facebook_username ) {
		// Facebook account
		echo "<div class='fb-page fb-root' data-href='https://www.facebook.com/$facebook_username' data-height='600' data-small-header='false' data-adapt-container-width='true' data-hide-cover='true' data-show-facepile='false' data-show-posts='true'></div>";
	}	else {
		// Facebook page
		echo "<div class='fb-page' data-href='$facebook_page' data-small-header='true' data-adapt-container-width='true' data-hide-cover='true' data-show-facepile='false' data-show-posts='true'><div class='fb-xfbml-parse-ignore'></div></div>";

	}
	echo "</div>"; // .facebook-content


	if ( strlen($tw_user) && strlen($tw_widgetid) ) {

		echo  // TWITTER
					"<div class='twitter-content'>";
			 			echo "<a class='twitter-timeline' href='https://twitter.com/$tw_user' data-widget-id='$tw_widgetid'>Tweets by @Estuaries4Life</a>";
		echo "</div>";
	}


	if ( strlen($instagram_user) ) {
		echo 	// INSTAGRAM
					"<div class='instagram-content'>";
		echo 		"No Instagram account yet established.<br/><br/>
						Of the 112 justices who have served on the Supreme Court from its beginning, only four were still on the Court at the age of 86.
						It therefore seems likely that we will see a substantial turnover on the Court over the course of the next decade.";
		echo "</div>";
	}

}

// Add class to indicate current secondary navigation (orange bar, menu)
function nerra_addclass_aboutus( $classes ) { // called by all pages in section
  $classes[] = 'aboutus';
  return $classes;
}
function nerra_addclass_page_primary( $classes ) { // called by each top-level nav page
  $classes[] = 'page-primary';
  return $classes;
}
function nerra_addclass_howwework( $classes ) { // called by all pages in section
  $classes[] = 'howwework';
  return $classes;
}
function nerra_addclass_whatweworkfor( $classes ) { // called by all pages in section
  $classes[] = 'whatweworkfor';
  return $classes;
}
function nerra_addclass_page_toolkit( $classes ) { // called by all pages in section
    $classes[] = 'howwework';
		$classes[] = 'page-toolkit';
  return $classes;
}

// Menu - Secondary - Which secondary menu to display
function nerra_select_secondary_menu( $args ) {
	if ( $args['theme_location'] != 'secondary' ) {
		return $args;
	}
	// compare pageid of top level page
	if ( is_in_tree( '256' ) ) { // How We Work, or any of its sub pages

		$args['menu'] = 'How We Work';

	} elseif ( is_in_tree( '21' ) ) { // What We Work For, or any of its sub pages

		$args['menu'] = 'What We Work For';

  } elseif ( is_in_tree( '461' ) ) { // About Us, or any of its sub pages

		$args['menu'] = 'About Us';
	}
	return $args;
}
function is_in_tree( $pid ) {

	// Returns true if $pid is this page or any of its sub pages
	global $post;

	if ( is_page( $pid ) ) {
		return true;  // we're at the page or at a sub page
	}
	$anc = get_post_ancestors( $post->ID );
	foreach ( $anc as $ancestor ) {
		if( is_page() && $ancestor == $pid ) {
			return true;
		}
	}
	return false;  // we aren't at the page, and the page is not an ancestor
}

// Footer
function nerra_display_footer() {
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));
	echo "<div class='hr'></div>";
	echo "<div class='group-noaa'>
					<div class='image'><img src='$path_to_resources/images/site/logo-noaa.png' width='74' height='74' alt='NOAA logo'></div>
					<div class='text'>
						The National Estuarine Research Reserve System<br />
						is a partnership between the National Oceanic and<br />
						Atmospheric Administration and coastal states.
					</div>
				</div>";


	echo "<div class='group-grassroots'>
					<div class='group'>
						<div class='text'>&#0169; ".  date("Y") . " <a href='#'>National Estuarine Research Reserve Association (NERRA)</a></div>
						<ul><li><a href='#'>contact</a></li><li><a href='/privacy-statement/'>privacy</a></li></ul>
					</div>
					<div class='image'><img src='$path_to_resources/images/site/Home_GrassrootsLogo.png' width='67' height='67' alt='Member of Grassroots'></div>
				</div>";

}


// TAXONOMIES
function nerra_create_all_custom_taxonomies() {


	// nerra_regions
	add_action( 'init', 'nerra_register_nerra_regions_taxonomy');
	add_filter( 'post_type_link', 'nerra_filter_nerra_regions', 10, 2);
	add_action( 'save_post', 'nerra_add_default_nerra_regions_term', 100, 2 );

	// nerra_project_categories
	add_action( 'init', 'nerra_register_nerra_project_categories');
	add_filter( 'post_type_link', 'nerra_filter_nerra_project_categories', 10, 2);
	add_action( 'save_post', 'nerra_add_default_nerra_project_categories_term', 100, 2 );

	// nerra_project_shortnames
	add_action( 'init', 'nerra_register_nerra_project_shortnames');
	add_filter( 'post_type_link', 'nerra_filter_nerra_project_shortnames', 10, 2);
	add_action( 'save_post', 'nerra_add_default_nerra_project_shortnames_term', 100, 2 );

	// nerra_people_categories
	add_action( 'init', 'nerra_register_nerra_people_categories');
	add_filter( 'post_type_link', 'nerra_filter_nerra_people_categories', 10, 2);
	add_action( 'save_post', 'nerra_add_default_nerra_people_categories_term', 100, 2 );

	// nerra_promo_block_positions
	add_action( 'init', 'nerra_register_nerra_promo_block_positions');
	add_filter( 'post_type_link', 'nerra_filter_nerra_promo_block_positions', 10, 2);
	add_action( 'save_post', 'nerra_add_default_nerra_promo_block_positions_term', 100, 2 );

	// nerra_document_placement
	add_action( 'init', 'nerra_register_nerra_document_placement');
	add_filter( 'post_type_link', 'nerra_filter_nerra_document_placement', 10, 2);
	add_action( 'save_post', 'nerra_add_default_nerra_document_placement_term', 100, 2 );
}

// Taxonomy - Reserve Regions
function nerra_register_nerra_regions_taxonomy() {
    register_taxonomy(
        'nerra_regions',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'nerra_reserve',    //post type name
        array(
            'hierarchical' => true,
            'label' => 'Reserve Regions',  //Display name
            'public' => true,
            'query_var' => true,
            'rewrite' => array( // REMEMBER to FLUSH AFTER A CHANGE - go to permalinks
                'slug' => 'reserves', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before
            )
        )
    );
    register_taxonomy_for_object_type( 'nerra_regions', 'nerra_reserve' ); // important to do per codex
}
function nerra_filter_nerra_regions($link, $post) {
  if ($post->post_type != 'nerra_reserve')
    	return $link;

  if ($cats = get_the_terms($post->ID, 'nerra_regions'))
      $link = str_replace('%nerra_regions%', array_pop($cats)->slug, $link);
  return $link;
}
function nerra_add_default_nerra_regions_term( $post_id, $post ) {
  if ( 'publish' === $post->post_status ) {
      $defaults = array(
          'nerra_regions' => array( 'Other'),   //

          );
      $taxonomies = get_object_taxonomies( $post->post_type );
      foreach ( (array) $taxonomies as $taxonomy ) {
          $terms = wp_get_post_terms( $post_id, $taxonomy );
          if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
              wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
          }
      }
  }
}

// Taxonomy - Project Categories
function nerra_register_nerra_project_categories() {
    register_taxonomy(
        'nerra_project_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'nerra_project',    //post type name
        array(
            'hierarchical' => true,
            'label' => 'Project Categories',  //Display name
            'public' => true,
            'query_var' => true,
            'rewrite' => array( // REMEMBER to FLUSH AFTER A CHANGE - go to permalinks
                'slug' => 'projects', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before
            )
        )
    );
    register_taxonomy_for_object_type( 'nerra_project_categories', 'nerra_project' ); // important to do per codex
}
function nerra_filter_nerra_project_categories($link, $post) {
  if ($post->post_type != 'nerra_project')
    	return $link;

  if ($cats = get_the_terms($post->ID, 'nerra_project_categories'))
      $link = str_replace('%nerra_project_categories%', array_pop($cats)->slug, $link);
  return $link;
}
function nerra_add_default_nerra_project_categories_term( $post_id, $post ) {
  if ( 'publish' === $post->post_status ) {
      $defaults = array(
          'nerra_project_categories' => array( 'Other'),   //
          );
      $taxonomies = get_object_taxonomies( $post->post_type );
      foreach ( (array) $taxonomies as $taxonomy ) {
          $terms = wp_get_post_terms( $post_id, $taxonomy );
          if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
              wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
          }
      }
  }
}

// Taxonomy - Project Shortnames - used by Project Resources to display and filter items in the admin
function nerra_register_nerra_project_shortnames() {
  //$project_posttypes = array("project_resource");
    register_taxonomy(
        'nerra_project_shortnames',  //Name of taxonomy.
        'project_resource',    			 //Name or array of post type(s)
        array(
            'hierarchical' => true,
            'label' => 'Project Names (short)',  //Display name
            'public' => true,
            'query_var' => true,
            'rewrite' => array( // REMEMBER to FLUSH AFTER A CHANGE - go to permalinks
                'slug' => 'project-shortnames', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before it
            )
        )
    );
    register_taxonomy_for_object_type( 'nerra_project_shortnames', 'project_resource' ); // important to do per codex
}
function nerra_filter_nerra_project_shortnames($link, $post) {
  if ($post->post_type != 'project_resource')
    	return $link;

  if ($cats = get_the_terms($post->ID, 'nerra_project_shortnames'))
      $link = str_replace('%nerra_project_shortnames%', array_pop($cats)->slug, $link);
  return $link;
}
function nerra_add_default_nerra_project_shortnames_term( $post_id, $post ) {
  if ( 'publish' === $post->post_status ) {
      $defaults = array(
          'nerra_project_shortnames' => array( 'Other'),   //
          );
      $taxonomies = get_object_taxonomies( $post->post_type );
      foreach ( (array) $taxonomies as $taxonomy ) {
          $terms = wp_get_post_terms( $post_id, $taxonomy );
          if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
              wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
          }
      }
  }
}

// Taxonomy - People Categories
function nerra_register_nerra_people_categories() {
    register_taxonomy(
        'nerra_people_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'nerra_people',    //post type name
        array(
            'hierarchical' => true,
            'label' => 'Serves on',  //Display name
            'public' => true,
            'query_var' => true,
            'rewrite' => array( // REMEMBER to FLUSH AFTER A CHANGE - go to permalinks
                'slug' => 'people', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before
            )
        )
    );
    register_taxonomy_for_object_type( 'nerra_people_categories', 'nerra_people' ); // important to do per codex
}
function nerra_filter_nerra_people_categories($link, $post) {
  if ($post->post_type != 'nerra_people')
    	return $link;

  if ($cats = get_the_terms($post->ID, 'nerra_people_categories'))
      $link = str_replace('%nerra_people_categories%', array_pop($cats)->slug, $link);
  return $link;
}
function nerra_add_default_nerra_people_categories_term( $post_id, $post ) {
  if ( 'publish' === $post->post_status ) {
      $defaults = array(
          'nerra_people_categories' => array( 'Other'),   //
          );
      $taxonomies = get_object_taxonomies( $post->post_type );
      foreach ( (array) $taxonomies as $taxonomy ) {
          $terms = wp_get_post_terms( $post_id, $taxonomy );
          if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
              wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
          }
      }
  }
}

// Taxonomy - Promo Block Positions
function nerra_register_nerra_promo_block_positions() {
    register_taxonomy(
        'nerra_promo_block_positions',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'promo_block',    //post type name
        array(
            'hierarchical' => true,
            'label' => 'Where to post',  //Display name
            'public' => true,
            'query_var' => true,
        )
    );
    register_taxonomy_for_object_type( 'nerra_promo_block_positions', 'promo_block' ); // important to do per codex
}
function nerra_filter_nerra_promo_block_positions($link, $post) {
  if ($post->post_type != 'promo_block')
    	return $link;

  if ($cats = get_the_terms($post->ID, 'nerra_promo_block_positions'))
      $link = str_replace('%nerra_promo_block_positions%', array_pop($cats)->slug, $link);
  return $link;
}
function nerra_add_default_nerra_promo_block_positions_term( $post_id, $post ) {
  if ( 'publish' === $post->post_status ) {
      $defaults = array(
          'nerra_promo_block_positions' => array( 'Other'),   //
          );
      $taxonomies = get_object_taxonomies( $post->post_type );
      foreach ( (array) $taxonomies as $taxonomy ) {
          $terms = wp_get_post_terms( $post_id, $taxonomy );
          if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
              wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
          }
      }
  }
}

// Taxonomy - Document Placement
function nerra_register_nerra_document_placement() {
    register_taxonomy(
        'nerra_document_placement',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'nerra_document',    //post type name
        array(
            'hierarchical' => true,
            'label' => 'Where to post',  //Display name
            'public' => true,
            'query_var' => true,
        )
    );
    register_taxonomy_for_object_type( 'nerra_document_placement', 'nerra_document' ); // important to do per codex
}
function nerra_filter_nerra_document_placement($link, $post) {
  if ($post->post_type != 'nerra_document')
    	return $link;

  if ($cats = get_the_terms($post->ID, 'nerra_document_placement'))
      $link = str_replace('%nerra_document_placement%', array_pop($cats)->slug, $link);
  return $link;
}
function nerra_add_default_nerra_document_placement_term( $post_id, $post ) {
  if ( 'publish' === $post->post_status ) {
      $defaults = array(
          'nerra_document_placement' => array( 'Other'),   //
          );
      $taxonomies = get_object_taxonomies( $post->post_type );
      foreach ( (array) $taxonomies as $taxonomy ) {
          $terms = wp_get_post_terms( $post_id, $taxonomy );
          if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
              wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
          }
      }
  }
}

// Documents
function nerra_get_documents_by_termid( $term_id = 0) {
	// Called by About NERRA, and by the Planning, Doing,
	// and Wrapping Up templates from the CP Toolkit

	/**
	 * Term ids (categories)
	 *
	 * Funding: 82
	 * Estuaries Day: 81
	 */
		$args = array(
			'post_type' => 'nerra_document',
			'orderby'   => 'meta_value_num',
			'meta_key'  => 'document_sort_order',
			'order'		=> 'asc',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'nerra_document_placement',
					'field'    => 'term_id',
					'terms'    => $term_id,
				),
			),
		);

		$loop = new WP_Query( $args );

		return $loop;
}

function nerra_display_documents( $loop = 0) {
	if ( $loop ) {
		echo "<ul class='bullet-list'>";
		if( $loop->have_posts() ) {
			while( $loop->have_posts() ): $loop->the_post();
				$title = get_the_title();
				$document = get_field('document_file');
				echo "<li>$title <a class='internal-link' href=".$document['url']." target='_blank'>view</a></li>";
			endwhile;
		}
		echo "</ul>";
	}
}
function nerra_display_toolkit_documents($loop = 0, $doctype = 'best practice') {
	if ( $loop ) {
		if( $loop->have_posts() ) {
			while( $loop->have_posts() ): $loop->the_post();
				$postid = get_the_ID();
				$title = get_the_title();
				$excerpt = get_the_excerpt();
				$text = get_field('document_text');
				$document = get_field('document_file');
				$type = get_field('document_toolkit_type');
				if ( 'resource' == strtolower($type) && strtolower($type) == strtolower($doctype) ) {
						echo "<div class='toolkit-document'>";
							echo "<h4>$title</h4>";
							echo "$text";
						echo "</div>"; // .toolkit-document
				} else {
					if ( strtolower($type) == strtolower($doctype) ) {
						echo "<div class='toolkit-document'>";
							echo "<h4>$title</h4>";
							echo "<p>$excerpt</p>";
							echo "<a class='more-show link-indicator hide' href='#'>More</a>";
							echo "<span class='more-content'><br />$text<a class='more-hide link-indicator hide' href='#'>Less</a></span>";
							echo "<span class='link-divider'></span><a class='link-indicator' href=".$document['url']." target='_blank'>view</a>";
						echo "</div>"; // .toolkit-document
					}
				}
			endwhile;
		}
	}
	wp_reset_postdata();
}

function nerra_modify_post_mime_types( $post_mime_types ) {

  // effects the media library admin only
  $post_mime_types['application/pdf'] = array( __( 'PDFs' ), __( 'Manage PDFs' ), _n_noop( 'PDF <span class="count">(%s)</span>', 'PDFs <span class="count">(%s)</span>' ) );
  $post_mime_types['application/vnd.openxmlformats-officedocument.wordprocessingml.document'] = array( __( 'DOCXs' ), __( 'Manage DOCXs' ), _n_noop( 'DOCX <span class="count">(%s)</span>', 'DOCXs <span class="count">(%s)</span>' ) );
  $post_mime_types['application/msword'] = array( __( 'DOCs' ), __( 'Manage DOCs' ), _n_noop( 'DOC <span class="count">(%s)</span>', 'DOCs <span class="count">(%s)</span>' ) );
  $post_mime_types['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'] = array( __( 'XLSXs' ), __( 'Manage XLSXs' ), _n_noop( 'XLSX <span class="count">(%s)</span>', 'XLSXs <span class="count">(%s)</span>' ) );

  return $post_mime_types;
}

function nerra_display_archive_documents($loop = 0, $doctype = 'best practice') {

	if ( $loop ) {
		if( $loop->have_posts() ) {
			echo "<ul>";
			while( $loop->have_posts() ): $loop->the_post();
				$title = get_the_title();
				$document = get_field('document_file');
				$type = get_field('document_toolkit_type');
				if ( strtolower($type) != 'resource' && strtolower($type) == strtolower($doctype) ) {
					echo "<li><a href=".$document['url']." target='_blank'>$title</a></li>";
				}
			endwhile;
			echo "</ul>";
		}
	}
	wp_reset_postdata();
}

function nerra_display_documenttypes($doctype){
	echo "<span>[ <a class='internal-link' href='/how-we-work/collaborative-project-toolkit/'>back to toolkit</a> ]</span>";

	echo "<h3>Planning</h3>";

	$loop = nerra_get_documents_by_termid(87); // frame your project
	nerra_display_archive_documents($loop, $doctype);

	$loop = nerra_get_documents_by_termid(88);
	nerra_display_archive_documents($loop, $doctype);

	$loop = nerra_get_documents_by_termid(89);
	nerra_display_archive_documents($loop, $doctype);

	$loop = nerra_get_documents_by_termid(90);
	nerra_display_archive_documents($loop, $doctype);


	echo "<h3>Doing</h3>";

	$loop = nerra_get_documents_by_termid(91);
	nerra_display_archive_documents($loop, $doctype);

	$loop = nerra_get_documents_by_termid(92);
	nerra_display_archive_documents($loop, $doctype);

	$loop = nerra_get_documents_by_termid(93);
	nerra_display_archive_documents($loop, $doctype);


	echo "<h3>Wrapping up</h3>";

	$loop = nerra_get_documents_by_termid(94);
	nerra_display_archive_documents($loop, $doctype);

	$loop = nerra_get_documents_by_termid(95);
	nerra_display_archive_documents($loop, $doctype);

	$loop = nerra_get_documents_by_termid(96);
	nerra_display_archive_documents($loop, $doctype);

	echo "<span>[ <a class='internal-link' href='/how-we-work/collaborative-project-toolkit/'>back to toolkit</a> ]</span>";

}

function nerra_display_toolkit_jumplinks(){
	echo "
		<ul class='doclist-menu'>GO TO:
		<li><a href='#bestpractices'>BEST PRACTICES</a></li>
		<li><a href='#casestudies'>CASE STUDIES</a></li>
		<li><a href='#resources'>RESOURCES</a></li>
		</ul>";
}

function nerra_google_analytics(){
	echo "
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-67031374-1', 'auto');
	  ga('send', 'pageview');
	</script>
	";

}


/*
function nerra_display_Google_search_form(){
	echo "
<script>
  (function() {
    var cx = '014989262005407380831:5i-qg-x3kb4';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>";
}
*/

function show_hook_location() { // development utility
	echo'CONTENT WILL DISPLAY HERE FOR THIS HOOK';
}


//* SEARCH
// Customize search form input box text
add_filter( 'genesis_search_text', 'sp_search_text' );
function sp_search_text( $text ) {
	return esc_attr( 'Search my blog...' );
}
// Customize search form input button text
add_filter( 'genesis_search_button_text', 'sp_search_button_text' );
function sp_search_button_text( $text ) {
	return esc_attr( 'Go' );
}
// Customize search form label
add_filter( 'genesis_search_form_label', 'sp_search_form_label' );
function sp_search_form_label ( $text ) {
	return esc_attr( 'Custom Label' );
}
//*/


 
