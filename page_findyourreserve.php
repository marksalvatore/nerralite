<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Template Name: Find Your Reserve
* Description: Custom page
*/

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_findyourreserve_page');
add_filter( 'body_class','nerra_addclass_page_primary' );

function nerra_display_findyourreserve_page() {
	
	global $post;
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1></header>
						<div class='text'>";
							the_content();
			echo "</div>";
		echo "</div>
					<div class='image'>";	
						the_post_thumbnail( 'full' );
		echo "</div>";
		echo "</section>";
		
		
		echo "<div class='map-label'>Browse NERRS<br/>Locations by:</div>";
		echo "<div class='page-tabs map-tab current-tab'>Map</div>";
		echo "<div class='page-tabs list-tab'>Region</div>";
		echo "<div class='margin-stretch'>";
			echo "<div class='tabbed-area'>";	
			echo "<div class='map-content'><div id='map'></div>";
		
		echo "</div>"; // end .map-content
		
	endwhile;

	nerra_display_reservelist();	

	echo "</div></div>"; // end .tabbed-area	.group			  	

}


function nerra_display_reservelist() {
		
	$nerra_regions = get_terms( 'nerra_regions', array(
 			'orderby'    => 'name',
 			'order'		=> 'asc',
 			'hide_empty' => 1,
 ) );
 
	echo "<div class='list-content'>";
	
	$i=0;
	
	foreach ($nerra_regions as $region) {
		
		$i++;
		if(1 == $i ) {
			echo "<div class='one-half first'>";
		} else if( 4 == $i ) {
			echo "<div class='one-half coltwo'>";			
		}
		echo "<div class='list-content-region'>";
		echo "<h2>$region->name</h2>";
		echo "</div>";

		$args = array(
			'post_type' => 'nerra_reserve',
 			'orderby'    => 'name',
 			'order'		=> 'asc',
			'tax_query' => array(
				array(
					'taxonomy' => 'nerra_regions',
					'field'    => 'term_id',
					'terms'    => $region->term_id,
				),
			),
		);
		
		   
		$loop = new WP_Query( $args );
		
		if( $loop->have_posts() ) {
			
			
			while( $loop->have_posts() ) : $loop->the_post();
					
				$title = get_field('reserve_shortname');
				$uri = get_permalink();				
				$state = get_field('reserve_vitals_state');
				
				echo "<div class='list-content-link'>";
					echo '&#149; <a href="' . $uri . '">' . trim($title);
					if ( ! empty($state) ) echo ', '.trim($state);
					echo '</a>';
				echo "</div>";

			endwhile;
			
		}
		if(3 == $i ) {
			echo "</div>"; // .one-half first column
		}
	} // foreach
	
	echo "</div>"; // .one-half  last column
	
	echo "</div>"; // .list-content 

}


genesis();


