<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

remove_action('genesis_loop', 'genesis_do_loop');
//remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action('genesis_loop', 'nerra_display_nerra_blog_item');

//* Add custom body class to the head
add_filter( 'body_class', 'nerra_insert_blogitem_body_class' );
function nerra_display_nerra_blog_item() {

	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));

	while ( have_posts() ) : the_post();

		$postid = get_the_ID();
		$date = DateTime::createFromFormat('Ymd', get_field('blog_date'));
		$title = get_the_title();
		$text = get_field('blog_story');

		echo "<div class='blog-column'>";

			echo "<h1>";
				echo $title;
			echo "</h1>";
			echo "<div class='date'>";
				echo $date->format('F j, Y');
			echo "</div>";
			echo "<div class='text'>";
		  	echo $text;
			echo "</div>";

		echo "</div>"; //.group

		echo "<div class='image'>";
			if ( has_post_thumbnail($postid) ) {
					the_post_thumbnail();
			} else {
				echo "<img src='$path_to_resources/images/site/News_Celebrate.png' width='353' height='260' alt='$title'>";
			}

		echo "</div>"; //.image

		//echo "<div class='clear'></div>";

	endwhile;

}

function nerra_insert_blogitem_body_class( $classes ) {

   $classes[] = 'blog-item';
   return $classes;
}




genesis();
