<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: CPToolkit
 *
 */


//is_page_template(array('page_cptoolkit_planning.php', 'page_cptoolkit_doing.php', 'page_cptoolkit_wrappingup.php') )


remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_hwwtoolkit');
add_filter( 'body_class','nerra_addclass_howwework' );
add_filter( 'body_class','nerra_addclass_page_toolkit' );


function nerra_display_hwwtoolkit() {
	
	while ( have_posts() ) : the_post();
	
		$title = get_the_title();
		
		// Overview
		echo "<section class='overview'>
						<header>";
						// Determine H1 or H2
						if ( preg_match('/planning/i', $title) || preg_match('/doing/i', $title) || preg_match('/wrapping/i', $title) ) {							
							echo "<h1>$title</h1>";
						} else {
							echo "<h2>$title</h2>";							
						}
			echo "</header>";
			echo "<div class='image'>";	
							the_post_thumbnail( 'full' );
			echo "</div>";

		echo "</section>";
	
		echo "<div class='overview-text'>";
						the_content();

				if ( is_page('collaborative-project-toolkit') ) {
					echo "<ul class='doclist-menu'>BROWSE ALL:
								<li><a href='/collaborative-project-toolkit-best-practices-archive/'>BEST PRACTICES</a></li>
								<li><a href='/collaborative-project-toolkit-case-studies-archive/'>CASE STUDIES</a></li>
								</ul>";
				}

		echo "</div>";

	
	endwhile;
	
	if ( is_page('frame-your-project') )   {
		nerra_display_toolkit_frameyourproject();
	}
	
	
}

function nerra_display_toolkit_frameyourproject(){
	
	echo "<div class='hr'></div>";
	
	echo "<h2>Best Practices</h2>";
}

genesis();