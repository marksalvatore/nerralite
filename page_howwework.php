<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Template Name: How We Work (HWW)
* Description: Used as a page template to show page contents, followed by a loop 
* through the "How We Work" category
*/

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_dsp_howwework_overview');
add_action('genesis_loop', 'nerra_dsp_howwework_promoblocks');
add_filter( 'body_class','nerra_addclass_howwework' );
add_filter( 'body_class','nerra_addclass_page_primary' );

function nerra_dsp_howwework_overview() {
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1></header>
						<div class='text'>";
							the_content();
			echo "</div>";
		echo "</div>
					<div class='image'>";	
						the_post_thumbnail( 'full' );
		echo "</div>";
		echo "</section>";

	endwhile;
}
function nerra_dsp_howwework_promoblocks() {
		
	// Note: $catids = array(77,78,79,80); // promo_block_positions (category ids)

		$promo_block_positions = get_terms( 'nerra_promo_block_positions', array(
	 			'orderby'    => 'name',
	 			'order'		=> 'asc',
	 			'hide_empty' => 1,
	 			) 
	 	);
	
	$i=0;
	
	//echo "<div class='promo-block-bgimage'></div>"; // to position background image of grasses
	echo "<div class='promo-block-area'>";
	
	foreach($promo_block_positions as $position){
		
		$i++;
		
		$args = array(
			'post_type' => 'promo_block',
 			'orderby'    => 'name',
 			'order'		=> 'asc',
			'tax_query' => array(
				array(
					'taxonomy' => 'nerra_promo_block_positions',
					'field'    => 'term_id',
					'terms'    => $position->term_id,
				),
			),
		);
		
		$loop = new WP_Query( $args );

		if( $loop->have_posts() ) {
			
			while( $loop->have_posts() ): $loop->the_post();

				$title = get_the_title();
				$caption = get_field('promo_block_caption');
				$url = get_field('promo_block_url');
				$image = get_field('promo_block_image');
				
				echo "<a href='$url'>";
		
					if ( $i % 2 > 0 ) {
						echo "<div class='promo-block one-half first'>";
					}
					else {
						echo "<div class='promo-block one-half'>";
					}
						
					echo "<div class='promo-block-text'>";
									
					echo "<div class='promo-block-title'>$title</div>";
					echo "<div class='promo-block-caption'>$caption</div>";
					echo "</div>"; // end promo-block-text
					echo "<div class='promo-block-image'>";?>
					
					<img src="<?php echo $image['url']; ?>" />	
					
					<?php echo "</div>"; // end promo-block-image
					echo "</div>"; // end promo-block
					
				echo "</a>";
			endwhile;	
		}
	}
	echo "</div>"; // end promo-block-area	
}


genesis();


