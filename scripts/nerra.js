jQuery(document).ready(function ($) {


// Facebook feed
(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if(d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));

// Twitter feed
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");

// Google search
(function() {var cx = '014989262005407380831:5i-qg-x3kb4';var gcse = document.createElement('script');gcse.type = 'text/javascript';gcse.async = true;gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//cse.google.com/cse.js?cx=' + cx;var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s); })();




/* Link logo to home
 -------------------- */
	$('.title-area').click(function(){
		window.location.href = "/";
	});


/* Social Media tabs
 ------------------- */
 	// Facebook to display as default feed
 	$('.facebook-tab span').addClass('active-tab');
	$('.twitter-content').hide();
	$('.instagram-content').hide();

	// Insert toolkit icon
	$("<div class='icon-toolkit'>&nbsp;</div>").insertAfter( '#wdg_specialrecentpostsfree-13 .srp-post-title' );


/* Map / List project tabs
 ------------------------- */
 	// Project Map to display as default tab
	$('.page-template-page_hwwcollaborative .map-content').show();
	$('.page-template-page_hwwcollaborative .list-content').hide();

	// Resource Map to display as default tab
	$('.page-template-page_findyourreserve .map-content').show();
	$('.page-template-page_findyourreserve .list-content').hide();


/* About NERRA / Leadership Bio cards
 ----------------------------------- */
	$('[class^="biography-"]').hide();
	$('.biography-default').show();
	$('.executive').hide();
	$('.governing').hide();

/* Training Project blocks
 ------------------------------- */
	$('.issue-description').hide();


/* Collaborative Project Toolkit More|Less
 ----------------------------------------- */
 	$('.more-content').addClass('hide');
	$('.more-show, .more-hide').removeClass('hide');
	$('.strategies').addClass('hide'); // hide all strategy summaries from view


/* Matrix toggle
 ------------------------------ */
 $('.matrix-toggle').click(function(){	$('#cptool-approach').removeClass('hide');});
 $('.close-matrix').click(function(){	$('#cptool-approach').addClass('hide');});



/**************************************************************************************************
 *                                        EVENT LISTENERS
 * ---------------------------------------------------------------------------------------------- */

	// Tabs: Switch among Facebook, Twitter, Instagram
	$('.facebook-tab').click(function(){
		$('.twitter-tab span').removeClass('active-tab');
		$('.instagram-tab span').removeClass('active-tab');
		$('.facebook-tab span').addClass('active-tab');
		$('.twitter-content').hide();
		$('.instagram-content').hide();
		$('.facebook-content').show();
	});

	$('.twitter-tab').click(function(){
		$('.facebook-tab span').removeClass('active-tab');
		$('.instagram-tab span').removeClass('active-tab');
		$('.twitter-tab span').addClass('active-tab');
		$('.facebook-content').hide();
		$('.instagram-content').hide();
		$('.twitter-content').show();
	});

	$('.instagram-tab').click(function(){
		$('.facebook-tab span').removeClass('active-tab');
		$('.twitter-tab span').removeClass('active-tab');
		$('.instagram-tab span').addClass('active-tab');
		$('.facebook-content').hide();
		$('.twitter-content').hide();
		$('.instagram-content').show();
	});

	// MAP | LIST - Collaborative Resources tabs, and Find Your Reserves
	$('.map-tab').click(function(){
		$('.current-tab').removeClass('current-tab');
		$('.map-tab').addClass('current-tab');
		$('.map-content').show();
		$('.list-content').hide();
	});
	$('.list-tab').click(function(){
		$('.current-tab').removeClass('current-tab');
		$('.list-tab').addClass('current-tab');
		$('.map-content').hide();
		$('.list-content').show();
	});

	// Leadership profile cards
	$('.card, .current-card').click(function(){
		postid = this.id;
		$('[class^="biography-"]').hide();
		$('.biography-' + postid).show();
		$('.current-card').removeClass('current-card').addClass('card');
		$('#' + postid).removeClass('card').addClass('current-card');

	});
	$('.executive-tab').click(function(){
		$('.current-tab').removeClass('current-tab');
		$('.executive-tab').addClass('current-tab');
		$('.staff').hide();
		$('.governing').hide();
		$('.executive').show();
	});
	$('.staff-tab').click(function(){
		$('.current-tab').removeClass('current-tab');
		$('.staff-tab').addClass('current-tab');
		$('.staff').show();
		$('.governing').hide();
		$('.executive').hide();
	});
	$('.governing-tab').click(function(){
		$('.current-tab').removeClass('current-tab');
		$('.governing-tab').addClass('current-tab');
		$('.staff').hide();
		$('.governing').show();
		$('.executive').hide();
	});


	// Training Projects (also referred to as "issues") roll-over affect
	$('.issue').hover(
			// mouse in
	  	function() {
				postid = this.id;
				$('#' + postid).find('.issue-title').hide();
				$('#' + postid).find('.issue-description').show();
	  },	// mouse out
	  		function() {
					$('#' + postid).find('.issue-description').hide();
					$('#' + postid).find('.issue-title').show();
	  }
	);

	// Collaborative Project Toolkit, More | Less
	$('.more-show').on('click', function(e) {
	  $(this).next('.more-content').removeClass('hide');
	  $(this).addClass('hide');
	  e.preventDefault();
	});
	$('.more-hide').on('click', function(e) {
	  var p = $(this).parent('.more-content');
	  p.addClass('hide');
	  p.prev('.more-show').removeClass('hide'); // Hide only the preceding "Read More"
	  e.preventDefault();
	});

	// Put focus on search field
	$('#searchbox').focus();


/* ****************** COLLABORATIVE TOOLKIT SURVEY ************************

	This is for CPToolkit Survey. The survey is a wp page that uses the page_cptoolkit_doing template. Javascript processes the survey by comparing answers to a matrix of possible answers, then returns an array of classnames to hide (matching the 22 strategies), and HIDES those in the set, so all the others display to the user. So the mapping matrix is a negative mapping. The strategy will hide from view if one of these answers is given.

*/
	$('.submitsurvey').on('click', function(e) {
		//alert('form submitted');
		function isInArray(value, array) {
			return array.indexOf(value) > -1;
		}

		var selectedQ1 = "";
		var selectedQ2 = "";
		var selectedQ3 = "";
		var selectedQ4 = "";
		var selected = 0;
		var answers = [ ];
		var strategies = { };
		var hideTheseClasses = [ ];
		var selectedQ3_array = [ ];
		var checkedvalue = 0;
		var matchinganswer = 0;
		var unanswered = false;

		// Question 1 - get answer
		var selected = $("input[type='radio'][name='howmany']:checked");
		if (selected.length > 0) {
		    selectedQ1 = selected.val();
		} else {
			unanswered = true;
		}

		// Question 2 - get answer
		var selected = $("input[type='radio'][name='howmuch']:checked");
		if (selected.length > 0) {
		    selectedQ2 = selected.val();
		} else {
			unanswered = true;
		}

		// Question 3 - get answer
		if ($('#what_1').is(":checked"))
		{
		  checkedvalue = $('#what_1').val();
		  selectedQ3_array.push(checkedvalue);
		}
		if ($('#what_2').is(":checked"))
		{
		  checkedvalue = $('#what_2').val();
		  selectedQ3_array.push(checkedvalue);
		}
		if ($('#what_3').is(":checked"))
		{
		  checkedvalue = $('#what_3').val();
		  selectedQ3_array.push(checkedvalue);
		}
		if ($('#what_4').is(":checked"))
		{
		  checkedvalue = $('#what_4').val();
		  selectedQ3_array.push(checkedvalue);
		}
		if ($('#what_5').is(":checked"))
		{
		  checkedvalue = $('#what_5').val();
		  selectedQ3_array.push(checkedvalue);
		}

		// Question 4 - get answer
		var selected = $("input[type='radio'][name='when']:checked");
		if (selected.length > 0) {
		    selectedQ4 = selected.val();
		} else {
			unanswered = true;
		}

		if (unanswered){
			return;
		}
		answers = [ selectedQ1, selectedQ2, selectedQ3_array, selectedQ4 ];

		// An array of survey answers that are not representative of that strategy
		// Each strategy name should match the slug for its WP record in Toolkit Strategies
		var strategies = {
			"surveys": {
			    "q1": "[1,2]",
			    "q2": "[2,3]",
			    "q3": "[1]",
			    "q4": "[]"
			},
			"workshops": {
			    "q1": "[1,2]",
			    "q2": "[1,2]",
			    "q3": "[1,2,3,4]",
			    "q4": "[]"
			},
			"social-media": {
			    "q1": "[1,2]",
			    "q2": "[2,3]",
			    "q3": "[1,4]",
			    "q4": "[]"
			},
			"citizen-science": {
			    "q1": "[1,2]",
			    "q2": "[2,3]",
			    "q3": "[1]",
			    "q4": "[]"
			},
			"experiential-engagement": {
			    "q1": "[1,3]",
			    "q2": "[1,3]",
			    "q3": "[4]",
			    "q4": "[]"
			},
			"focus-groups": {
			    "q1": "[2,3]",
			    "q2": "[2,3]",
			    "q3": "[1,2]",
			    "q4": "[]"
			},
			"gallery-walk": {
			    "q1": "[1,2]",
			    "q2": "[1,3]",
			    "q3": "[1,4,5]",
			    "q4": "[]"
			},
			"graphic-facilitation": {
			    "q1": "[1,2]",
			    "q2": "[1,3]",
			    "q3": "[1,2]",
			    "q4": "[]"
			},
			"keypad-polling": {
			    "q1": "[1,2]",
			    "q2": "[2,3]",
			    "q3": "[1,3]",
			    "q4": "[]"
			},
			"open-house": {
			    "q1": "[1,2]",
			    "q2": "[1,3]",
			    "q3": "[4]",
			    "q4": "[]"
			},
			"role play": {
			    "q1": "[2,3]",
			    "q2": "[1,3]",
			    "q3": "[2,4,5]",
			    "q4": "[]"
			},
			"samoan-circle": {
			    "q1": "[1,2]",
			    "q2": "[2,3]",
			    "q3": "[1,5]",
			    "q4": "[]"
			},
			"design-charette": {
			    "q1": "[1,2]",
			    "q2": "[1,3]",
			    "q3": "[1,2]",
			    "q4": "[2,3]"
			},
			"key-informant-interviews": {
			    "q1": "[2,3]",
			    "q2": "[2,3]",
			    "q3": "[1]",
			    "q4": "[2,3]"
			},
			"kitchen-table-discussion": {
			    "q1": "[2,3]",
			    "q2": "[2,3]",
			    "q3": "[1,2,5]",
			    "q4": "[2,3]"
			},
			"stakeholder-advisory-committee": {
			    "q1": "[2,3]",
			    "q2": "[2,3]",
			    "q3": "[1,2,3]",
			    "q4": "[2,3]"
			},
			"technical-advisory-committee": {
			    "q1": "[2,3]",
			    "q2": "[2,3]",
			    "q3": "[1,2,3]",
			    "q4": "[2,3]"
			},
			"visioning": {
			    "q1": "[1,2]",
			    "q2": "[1,3]",
			    "q3": "[1,2,3,4]",
			    "q4": "[2,3]"
			},
			"participatory-mapping": {
			    "q1": "[1,2]",
			    "q2": "[2,3]",
			    "q3": "[1,2]",
			    "q4": "[1,3]"
			},
			"beta-testing": {
			    "q1": "[1,3]",
			    "q2": "[1,2]",
			    "q3": "[1]",
			    "q4": "[1,2]"
			},
			"technical-assistance": {
			    "q1": "[2,3]",
			    "q2": "[1,3]",
			    "q3": "[4]",
			    "q4": "[1,2]"
			},
			"tools-cafe": {
			    "q1": "[1,2]",
			    "q2": "[1,3]",
			    "q3": "[4]",
			    "q4": "[1,2]"
			}
		};


		// Compare the answer map to the actual survey answers
		$.each(strategies, function(index) {
	    var i = 0;
	    $.each(strategies[index], function(key, arr) {
	      //console.log('key: ' + key + ' arr: ' + arr + ' index: ' + index + ' i = ' + i);
	      if ( i != 2 ) { // question three uses checkboxes
	        if (isInArray(answers[i], arr) ) {
		        	// if the survey answer matches an unacceptable answer for this strategy
		        	// exclude it from display by adding it to this array.
		        	if ( ! isInArray(answers[i], hideTheseClasses) ) {
		        		hideTheseClasses.push(index);
		        	}
	        }
	      } else {
		      //question 3: checkboxes
		      $.each( selectedQ3_array, function( intValue, currentElement ) {
		        if (isInArray(currentElement, arr) ) {
			        	// if currentElement is in arr, this strategy shouldn't be excluded (hidden)
			        	matchinganswer = 1;
		        }
					});

					if ( matchinganswer == 0 ) {
							// Add it to hideTheseClasses if not already there
							if ( ! isInArray(answers[i], hideTheseClasses) ) {
		        		hideTheseClasses.push(index);
		        	}
					}
	      }
	      i++;
	    });


		}); // .each


		// Remove dupes
		var uniqueNames = [];
		$.each(hideTheseClasses, function(i, el){
		    if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
		});
		hideTheseClasses = uniqueNames;

		// Swap out survey form heading for survey report heading
		$('.overview-text-background h2').hide();
		$('.overview-text-background p').hide();
		$('h3.survey-report').removeClass('hide');
		$('span.survey-report').removeClass('hide');

		// Hide the survey form
		$('#survey-form').addClass('hide');


		// Console style used in console.log
		//var mycss = "color:green; font-size: 12px; font-weight: bold;";
		//console.log('%c' + hideTheseClasses.length + ' strategies to be hidden:', mycss);


		// Hide the strategies that aren't relevant
		$.each( hideTheseClasses, function( intValue, currentElement ) {
			//console.log(currentElement);
			$('.' + currentElement).hide()
		});

		// Show the strategies that aren't hidden (that apply to this survey)
		$('.strategies').removeClass('hide');
		//$('.toolkit-menu').hide();
		$('.toolkit-content-survey h3').hide();


	});	// submit survey






}); // end jquery

