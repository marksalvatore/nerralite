<?php

add_action( 'genesis_before_loop', 'genesis_do_search_title' );

function genesis_do_search_title() {

	$search_terms = get_search_query();
	$search_header = "
		<section class='overview'>
			<div class='group'>
				<header><h1>Search results for: <span class='sterm'>$search_terms</span></h1></header>
			</div>
		</section>";

	$title = sprintf( $search_header, apply_filters( 'genesis_search_title_text', __( 'Search results for:', 'genesis' ) ), '<span class=sterm>'.get_search_query().'</span>' );

	echo apply_filters( 'genesis_search_title_output', $title ) . "\n";

}



genesis();
