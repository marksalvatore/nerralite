<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

remove_action('genesis_loop', 'genesis_do_loop');
//remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action('genesis_loop', 'nerra_display_nerra_news_item');

//* Add custom body class to the head
add_filter( 'body_class', 'nerra_insert_newsitem_body_class' );
function nerra_display_nerra_news_item() {
	
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));	

	while ( have_posts() ) : the_post();

		$postid = get_the_ID();
		$date = DateTime::createFromFormat('Ymd', get_field('news_date'));
		$title = get_the_title();
		$text = get_field('news_story');

		
		echo "<div class='news-column'>";

			echo "<h1>";
				echo $title;
			echo "</h1>";

			echo "<a class='back-to-blog' href='nerra-news-page'>- back to blog -</a>";

			echo "<div class='date'>";
				echo $date->format('F j, Y');
			echo "</div>";
			echo "<div class='text'>";
		  	echo $text;
			echo "</div>";

			echo "<div class='news-categories'>";
			echo "On ".$date->format('F j, Y')." / ";
			get_cats();
			echo "</div>";

			echo "<a class='back-to-blog' href='nerra-news-page'>- back to blog -</a>";
			
		echo "</div>"; //.group

/*	Out of play since April 13, 2017. Jill's request 

		echo "<div class='image'>";
			if ( has_post_thumbnail($postid) ) {
					the_post_thumbnail();
			} else {
				echo "<img src='$path_to_resources/images/site/News_Celebrate.png' width='353' height='260' alt='$title'>";
			}
		echo "</div>"; //.image
*/
	
		/*echo "<div class='clear'></div>";*/

	endwhile;
	
}

function nerra_insert_newsitem_body_class( $classes ) {
	
   $classes[] = 'news-item';
   return $classes; 
}




genesis();
