/* ************************** MAPBOX: Projects Map ******************************************
 * Controls the projects map on the How We Work: Collaborative Research page. 
 * Tegan's javascript: https://jsfiddle.net/cj4L2xxs/9
 */

L.mapbox.accessToken = 'pk.eyJ1IjoidGVnYW5lbGxvbmkiLCJhIjoiRXBEdHdJVSJ9.qVv2Fe8USwBPrVZwiPOgIg';
var map = L.mapbox.map('map', 'teganelloni.NERRS_Projects_Basemap')
    .setView ([39.08, -98.855], 4);

var myLayer = L.mapbox.featureLayer().addTo(map);

var geoJson = [
    {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-70.500,43.100]},
    "properties": {
        "image": "http://mediad.publicbroadcasting.net/p/nhpr/files/styles/x_large/public/201112/greatbay081810-01.jpg",
        "url": "http://50.87.232.11/projects/tracking-nitrogen-pollution-in-new-hampshires-great-bay",
        "marker-color": "#ececec",
        "marker-size": "small",
        "marker-url":"http://www.wellsreserve.org/",
        "city": "Tracking nitrogen pollution in New Hampshire’s Great Bay"
    }
}, 
    
        {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-70.3784,43.2612]},
    "properties": {
        
        "url": "http://50.87.232.11/projects/valuing-the-ecosystem-services-of-southern-maine-watersheds",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Valuing the ecosystem services of southern Maine watersheds"
    }
}, 
        {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-70.999,43.000]},
    "properties": {
        "image": "http://www.wellsreserve.org/writable/images/ctp/simulation.jpeg",
        "marker-color": "#ececec",
        "marker-size": "small",
        "url":"http://50.87.232.11/projects/game-changer-new-england-climate-change-adaptation-project/",
        "city": "Game changer: New England climate change adapatation project"
    }
}, 
    
    
        {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-70.300,42.800]},
    "properties": {
        "image": "http://www.wellsreserve.org/writable/images/ctp/simulation.jpeg",
        "marker-color": "#ececec",
        "marker-size": "small",
        "url":"http://50.87.232.11/projects/watershed-planning-across-boundaries-in-new-hampshire/",
        "city": "Watershed planning across boundaries in New Hampshire"
    }
}, 

    {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-70.855,43.0804]},
    "properties": {
        "image": "http://www.wellsreserve.org/writable/images/fishladder.jpg",
        "url": "http://50.87.232.11/projects/green-infrastructure-for-a-sustainable-new-hampshire",
        "marker-color": "#ececec",
         "marker-size": "small",
        "city": "Green infrastructure for a sustainable New Hampshire"
    }
}, 
    
        {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-71.500,42.800]},
    "properties": {
        "image": "http://greatbay.org/graphics/photos/wetland-wandering.jpg",
        "url": "http://50.87.232.11/projects/community-based-adaptation-planning-for-new-hampshire",
        "marker-color": "#ececec",
         "marker-size": "small",
        "city": "Community-based adaptation planning for New Hampshire"
    }
}, 
    
    
    {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-70.513, 41.560]},
    "properties": {
        "image": "http://www.waquoitbayreserve.org/wp-content/uploads/2013/02/DSC_0110-275x182.jpg",
        "url": "http://50.87.232.11/projects/bringing-wetlands-to-market-in-massachusetts",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Bringing wetlands to market in Massachusetts"
    }
},
    

       {    
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-73.7856,40.700]},    
    "properties": {
        "image":     "https://www.hrnerr.org/wp-content/uploads/sites/9/2014/07/Shorelines1-300x300.jpg",
        "url": "http://50.87.232.11/projects/sustainable-shorelines-for-new-yorks-hudson-river/",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Sustainable shorelines for New York’s Hudson River"
    }
},

            {    
    type: 'Feature',
                "geometry": { "type": "Point", "coordinates": [-74.3549,39.4956  ]},
                "properties": {
        "image":     "http://marine.rutgers.edu/main/images/stories/chris_with_shark.jpg",
                    "url": "http://50.87.232.11/projects/connecting-the-dots-between-data-and-atlantic-fisheries-management",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Connecting the dots between data and Atlantic fisheries management"
    }
},

                {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-80.200,32.400]},
                "properties": {
        "image":     "http://www.photolib.noaa.gov/700s/nerr0045.jpg",
                    "url": "http://50.87.232.11/projects/assessing-habitat-vulnerability-in-a-time-of-change/",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Assessing habitat vulnerability in a time of change"
    }
},

                   {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-76.000, 38.000]},
                "properties": {
        "image":     "http://www.epa.gov/reg3artd/images/annapolis_new.jpg",
                    "url": "http://50.87.232.11/projects/enhancing-resilience-on-marylands-deal-island",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Enhancing resilience on Maryland’s Deal Island"
    }
},
                    {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-82.5087,41.3521]},
                "properties": {
        "image":     "http://wildlife.ohiodnr.gov/portals/wildlife/images/public%20areas/owc1.jpg",
                    "url": "http://50.87.232.11/projects/innovative-stormwater-treatment-in-ohio",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Innovative stormwater treatment in Ohio"
    }
},   
    
                     {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-77.8395,34.1164]},
                "properties": {
        "image":     "http://www.cakex.org/sites/default/files/nerr0085.jpg",
                    "url": "http://50.87.232.11/projects/stormwater-solutions-for-north-carolina",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Stormwater solutions for North Carolina"
    }
},   
        
                             {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-79.5081,33.0270]},
                "properties": {
        "image":     "http://www.dnr.sc.gov/marine/NERR/images/perviouscobblestone.JPG",
                    "url": "http://50.87.232.11/projects/advancing-low-impact-development-in-coastal-south-carolina",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Advancing Low Impact Development in Coastal South Carolina"
    }
},   
                         {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-79.900,32.800]},
                "properties": {
        "image":     "http://www.dnr.sc.gov/marine/NERR/images/bioswaleshot.JPG",
                    "url": "http://50.87.232.11/projects/modeling-stormwater-impacts-in-coastal-south-carolina",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Modeling stormwater impacts in Coastal South Carolina"
    }
}, 
    
                                 {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-79.200,33.500]},
                "properties": {
        "image":     "http://seagrant.noaa.gov/Portals/0///EasyDNNNews/205/757475p715EDNmain2051KiawahPondTopPhoto1%C2%A9beahmCropped_cropforweb.jpg",
                    "url": "http://50.87.232.11/projects/understanding-south-carolinas-swash-cycle/",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Understanding South Carolina’s “swash cycle”"
    }
},  
                                                      {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-79.500,33.0000]},
                "properties": {
        "image":     "http://broome.soil.ncsu.edu/seaside_elder1.jpg",
                    "url": "http://50.87.232.11/projects/bringing-shorelines-to-life-in-south-carolina/",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Bringing shorelines to life in South Carolina"
    }
},  
    
    

    
        {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-81.2782,29.9645]},
                "properties": {
        "image":     "http://augustine.com/sites/default/files/styles/cover/public/gtmnerr_maritime_hammock_5x3.jpg?itok=PP6lUBLu",
            "url": "http://50.87.232.11/projects/planning-for-floridas-rising-tides/",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Planning for Florida's rising tides"
    }
},   
    
    
            {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-81.6298,25.8790]},
                "properties": {
        "image":     "http://www.fyccn.com/sites/default/files/images/rookery_bay_national_estuary_research_preserve.450%20wide.jpg",
            "url": "http://50.87.232.11/projects/managing-freshwater-for-the-future-in-florida/",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Managing freshwater for the future in Florida"
    }
},   
    
     
    
        {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-87.833,30.3705]},
                "properties": {
        "image":     "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSlyQWHU48r65qqevNbYQNK4WtnvM5t0G-Vp12pT4RNvyts8yue",
            "url": "http://50.87.232.11/projects/protecting-alabamas-water-quality-through-marsh-restoration",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Protecting Alabama’s water quality through marsh restoration"
    }
},   
    
    
            {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-88.4225,30.3616]},
                "properties": {
        "image":     "http://grandbaynerr.org/wp-content/uploads/2013/12/sightings-grand-bay-nerr-feature.jpg",
            "url": "http://50.87.232.11/projects/planning-the-future-by-exploring-the-past-in-mississippis-grand-bay",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Planning the future by exploring the past in Mississippi’s Grand Bay"
    }
},   
    
    
                {
    type: 'Feature',
               "geometry": { "type": "Point", "coordinates": [-96.9448,28.1350]},
                "properties": {
								"image":     "http://www.missionaransas.org/images/education_community_4_crop.jpg",
                "url": "http://50.87.232.11/projects/balancing-freshwater-needs-in-texas-changing-climate",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Balancing freshwater needs in Texas’ changing climate"
    }
},   
    
                    {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-117.1108,32.5433]},
                "properties": {
        "image":     "http://media.sdreader.com/img/events/2015/A.Noto_web_t240.jpg?9b075e176a263354460210e5f64f6db9d4623575",
                    "url": "http://50.87.232.11/projects/healthy-wetlands-and-communities-for-southern-california",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Healthy wetlands and communities for Southern California"
    }
},   
    
                        {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-121.734,36.8093]},
                "properties": {
        "image":     "http://www.sfbaysubtidal.org/images/oyster_tray.jpg",
                    "url": "http://50.87.232.11/projects/a-future-for-oysters-along-the-pacific-coast",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "A future for oysters along the Pacific Coast"
    }
},   
                        {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [ -122.4883,38.0010]},
                "properties": {
        "image":     "http://www.southwestclimatechange.org/files/cc/figures/monitoring-equipment.jpg",
                    "url": "http://50.87.232.11/projects/our-coast,-our-future:-planning-for-climate-change-in-san-francisco",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Our coast, our future: planning for climate change in San Francisco"
    }
},  
                {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [ -123.000,38.0010]},
                "properties": {
        "image":     "https://baynature.org/wp-content/uploads/2014/04/14-117_Kimmel_6834.jpg",
                    "url": "http://50.87.232.11/projects/mud-on-the-move-predicting-how-marshes-will-change-as-sea-levels-rise/",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Mud on the Move: New Approaches to Sampling Suspended Sediment"
    }
},      
 
    
                            {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [ -124.312,43.2912]},
                "properties": {
        "image":     "http://imgick.oregonlive.com/home/olive-media/width960/img/oregonian/photo/2014/05/-0e67d3c62d85a036.JPG",
                    "url": "http://50.87.232.11/projects/bringing-the-oly-oyster-back-to-oregons-coast/",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Bringing the Oly Oyster back to Oregon's Coast"
    }
},  
                        {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [ -124.312,43.6]},
                "properties": {
        "image":     "http://www.oregon.gov/dsl/SSNERR/PublishingImages/researchskiff.JPG",
                    "url": "http://50.87.232.11/projects/building-partnerships-for-oregons-coastal-watershed",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Building partnerships for Oregon's coastal watersheds"
    }
},  
    
 
                                {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-92.061,46.6758]},
                "properties": {
        "image":     "http://lsnerr.uwex.edu/img/photo/WCMPpokegama-2.jpg",
                    "url": "http://50.87.232.11/projects/protecting-wetlands-for-the-future-in-wisconsin/",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Protecting wetlands for the future in Wisconsin"
    }
},      
    
                                   {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-128.2324,54.6738]},
                "properties": {
        "image":     "https://kbaycouncil.files.wordpress.com/2010/02/recruit01.jpg?w=600",
                    "url": "http://50.87.232.11/projects/preparing-for-alaskas-changing-landscape/",
        "marker-color": "#ececec",
        "marker-size": "small",
        "city": "Preparing for Alaska's changing landscape"
    }
},      
  
    
     
     
    
];
    
    
    
// CUSTOM POPUPS
myLayer.on('layeradd', function(e) {
    var marker = e.layer,
        feature = marker.feature;

    // Create custom popup content
    var popupContent =  '<a class="popup" href="' + feature.properties.url + '">' +
                            
                            feature.properties.city +
                        '</a>';

    // http://leafletjs.com/reference.html#popup
    marker.bindPopup(popupContent,{
        closeButton: false,
        minWidth: 320,
   
    });
});

// ADD FEATURES
myLayer.setGeoJSON(geoJson);
    myLayer.on('mouseover', function(e) {
    e.layer.openPopup();
});
   myLayer.on('click', function(e) { window.open(e.layer.feature.properties.url);
});