/* ************************** MAPBOX: Reserves Map ******************************************
 * Controls the reserves map on the Find Your Reserve page. 
 * Tegan's javascript: https://jsfiddle.net/7413mj22/20
 */

L.mapbox.accessToken = 'pk.eyJ1IjoidGVnYW5lbGxvbmkiLCJhIjoiRXBEdHdJVSJ9.qVv2Fe8USwBPrVZwiPOgIg';
var map = L.mapbox.map('map', 'teganelloni.004d0ab8')
    .setView ([39.08, -98.855], 4);

var myLayer = L.mapbox.featureLayer().addTo(map);

var geoJson = [

    
    {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-70.3784,43.2612]},
    "properties": {
        "image": "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack1/stack_one_hero_c_wells.png",
        "url": "http://50.87.232.11/reserves/wells-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "marker-url":"http://www.wellsreserve.org/",
        "city": "Wells National Estuarine Research Reserve"
    }
}, 

    {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-70.855,43.0804]},
    "properties": {
        "image": "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack2/stack_two_hero_e_great_bay_spartina.png",
        "url": "http://50.87.232.11/reserves/great-bay-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
         "marker-size": "small",
        "city": "Great Bay National Estuarine Research Reserve"
    }
}, 
    
    
    {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-70.513, 41.560]},
    "properties": {
        "image": "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack4/stack_four_hero_f_waquoit.png",
        "url": "http://50.87.232.11/reserves/waquoit-bay-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Waquoit Bay National Estuarine Research Reserve"
    }
},
    

    {
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-71.320,41.621]},
    "properties": {
        "image": "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack4/stack_four_hero_e_narragansett.png",
        "url": "http://50.87.232.11/reserves/narragansett-bay-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Narragansett Bay National Estuarine Research Reserve"
    }
},
    
    {    
    type: 'Feature',
    "geometry": { "type": "Point", "coordinates": [-73.7856,40.9882]},    
    "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack3/stack_three_hero_c_hudsonriver.png",
        "url": "http://50.87.232.11/reserves/hudson-river-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Hudson River National Estuarine Research Reserve"
    }
},

        {    
    type: 'Feature',
            "geometry": { "type": "Point", "coordinates": [-75.3706, 39.0533]}  , 
    "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack1/stack_one_hero_c_wells.png",
        "url": "http://50.87.232.11/reserves/delaware-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Delaware National Estuarine Research Reserve"
    }
},

            {    
    type: 'Feature',
                "geometry": { "type": "Point", "coordinates": [-74.3549,39.4956  ]},
                "properties": {
                    "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack2/stack_two_hero_d_jacques_cousteau.png",
                    "url": "http://50.87.232.11/reserves/jacques-cousteau-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Jacques Cousteau National Estuarine Research Reserve"
    }
},

                {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-76.2685, 39.447]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack3/stack_three_hero_f_chesapeake_maryland.png",
                    "url": "http://50.87.232.11/reserves/chesapeake-bay-national-estuarine-research-reserve-maryland/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Chesapeake Bay MD National Estuarine Research Reserve"
    }
},

                   {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-76.3893,37.2216]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack1/stack_one_hero_c_wells.png",
                    "url": "	http://50.87.232.11/reserves/chesapeake-bay-national-estuarine-research-reserve-virginia/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Chesapeake Bay VA National Estuarine Research Reserve"
    }
},

                    {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-82.5087,41.3521]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack4/stack_four_hero_c_oldwomancreek.png",
                    "url": "	http://50.87.232.11/reserves/old-woman-creek-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Old Woman Creek National Estuarine Research Reserve"
    }
},   
    
                     {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-77.8395,34.1164]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack4/stack_four_hero_b_northcarolina.png",
                    "url": "	http://50.87.232.11/reserves/north-carolina-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "North Carolina National Estuarine Research Reserve"
    }
},   
    
   
                         {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-79.2128,33.3305]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack5/stack_five_hero_c_north_inlet.png",
                    "url": "	http://50.87.232.11/reserves/north-inlet-winyah-bay-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "North Inlet - Winyah Bay National Estuarine Research Reserve"
    }
},   
    
    
                             {    
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-80.4433,32.5376]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack3/stack_three_hero_d_ace.png",
                    "url": "	http://50.87.232.11/reserves/ace-basin-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "ACE Basin National Estuarine Research Reserve"
    }
},   
    
    {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-81.2892,31.4380]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/uploads/2015/04/sap_findreserve_intro_text.png",
            "url": "http://50.87.232.11/reserves/sapelo-island-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Sapelo Island National Estuarine Research Reserve"
    }
},   
    
    
        {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-81.2782,29.9645]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack3/stack_three_hero_a_gtm.png",
            "url": "http://50.87.232.11/reserves/guana-tolomato-matanzas-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Guana Tolomato Matanzas National Estuarine Research Reserve"
    }
},   
    
    
            {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-81.6298,25.8790]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack2/stack_two_hero_f_rookery_bay.png",
            "url": "	http://50.87.232.11/reserves/rookery-bay-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Rookery Bay National Estuarine Research Reserve"
    }
},   
    
    {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-84.9587,29.7548]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/uploads/2015/07/stack_one_hero_b_apalachicola.png",
            "url": "http://50.87.232.11/reserves/apalachicola-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Apalachicola National Estuarine Research Reserve"
    }
},   
    
        {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-87.833,30.3705]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack2/stack_two_hero_c_weeks.png",
            "url": "http://50.87.232.11/reserves/weeks-bay-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Weeks Bay National Estuarine Research Reserve"
    }
},   
    
    
            {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-88.4225,30.3616]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack2/stack_two_hero_b_grand_bay.png",
            "url": "http://50.87.232.11/reserves/grand-bay-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Grand Bay National Estuarine Research Reserve"
    }
},   
    
    
                {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-96.9448,28.1350]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack3/stack_three_hero_b_missionaransas.png",
                    "url": "	http://50.87.232.11/reserves/mission-aransas-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Mission-Aransas National Estuarine Research Reserve"
    }
},   
    
                    {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-117.1108,32.5433]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack3/stack_three_hero_b_tijuana.png",
                    "url": "	http://50.87.232.11/reserves/tijuana-river-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Tijuana River National Estuarine Research Reserve"
    }
},   
    
                        {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-121.734,36.8093]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack1/stack_one_hero_e_elkhorn.png",
                    "url": "	http://50.87.232.11/reserves/elkhorn-slough-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Elkhorn Slough National Estuarine Research Reserve"
    }
},   
                        {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [ -122.4883,38.0010]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack3/stack_three_hero_e_sfbay.png",
                    "url": "	http://50.87.232.11/reserves/san-francisco-bay-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "San Francisco Bay National Estuarine Research Reserve"
    }
},   
 
    
    
                            {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [ -124.312,43.2912]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack5/stack_five_hero_b_south_slough.png",
                    "url": "	http://50.87.232.11/reserves/south-slough-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "South Slough National Estuarine Research Reserve"
    }
},  
    
                            {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [ -122.477,48.5143]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack2/stack_two_hero_a_padilla_bay.png",
                    "url": "	http://50.87.232.11/reserves/padilla-bay-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Padilla Bay National Estuarine Research Reserve"
    }
},      
                                {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-92.061,46.6758]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack5/stack_five_hero_d_lakes_superior.png",
                    "url": "	http://50.87.232.11/reserves/lake-superior-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Lake Superior National Estuarine Research Reserve"
    }
},      
    
                                   {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-128.2324,54.6738]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack1/stack_one_hero_a_kachemak.png",
                    "url": "	http://50.87.232.11/reserves/kachemak-bay-national-estuarine-research-reserve/",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Kachemak Bay National Estuarine Research Reserve"
    }
},      
  
    
                       {
    type: 'Feature',
                    "geometry": { "type": "Point", "coordinates": [-76.6406,22.1874]},
                "properties": {
        "image":     "http://50.87.232.11/wp-content/themes/nerra-ark/images/heros/stack4/stack_four_hero_a_jobosbay.png",
                    "url": "	http://50.87.232.11/reserves/jobos-bay-national-estuarine-research-reserve",
        "marker-color": "#c35329",
        "marker-size": "small",
        "city": "Jobos Bay National Estuarine Research Reserve"
    }
},      
     
    
];
    
    
    
// CUSTOM POPUPS
myLayer.on('layeradd', function(e) {
    var marker = e.layer,
        feature = marker.feature;

    // Create custom popup content
    var popupContent =  '<a class="popup" href="' + feature.properties.url + '">' +
                            '<img src="' + feature.properties.image + '" />' +
                            feature.properties.city +
                        '</a>';

    // http://leafletjs.com/reference.html#popup
    marker.bindPopup(popupContent,{
        closeButton: false,
        minWidth: 320,
   
    });
});

// ADD FEATURES
myLayer.setGeoJSON(geoJson);
    myLayer.on('mouseover', function(e) {
    e.layer.openPopup();
});
myLayer.on('click', function(e) {
    window.open(e.layer.feature.properties.url);
});