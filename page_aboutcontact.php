<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: About Contact
 *
 */

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_aboutcontact_page');
add_filter( 'body_class','nerra_addclass_aboutus' );


function nerra_display_aboutcontact_page() {


	while ( have_posts() ) : the_post();
	
		echo "<section class='overview'>
					<header><h1>";
					
					the_title();
					
		echo "</h1></header><div class='text'>";
					
					the_content();
					
		echo "</div></header>";
		echo "</section>";
		
	endwhile;

}

genesis();