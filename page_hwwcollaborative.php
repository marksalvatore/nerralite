<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Template Name: HWW Collaborative Research
* Description: Custom page
*/

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_collaborative_page');
add_filter( 'body_class','nerra_addclass_howwework' );

function nerra_display_collaborative_page() {
	
	global $post;
	
	while ( have_posts() ) : the_post();
	
		echo "<section class='overview'>
						<header><h1>";
						the_title();					
		echo "</h1></header><div class='text'>";
						the_content();
			echo "</div>";
		echo "</section>";
				
			echo "<div class='map-label'>Browse Collaborative<br/>Research Projects by:</div>";
			echo "<div class='page-tabs map-tab current-tab'>Map</div>";
			echo "<div class='page-tabs list-tab'>Issues</div>";
			echo "<div class='margin-stretch'>";
				echo "<div class='tabbed-area'>";	
				echo "<div class='map-content'><div id='map'></div>";
	
		echo "</div>"; // end .map-content

	endwhile;

	nerra_display_projectlist();	
			  
	echo "<div class='collaborative-toolkit'></div>"; // pulls in default toolkit image
	echo "</div></div>"; // end .tabbed-area	.group
}


function nerra_display_projectlist() {
	
	$project_categories = get_terms( 'nerra_project_categories', array(
 			'orderby'    => 'name',
 			'order'		=> 'desc',
 			'hide_empty' => 1,
 			) 
 	);
  
	echo "<div class='list-content'>";
	
	foreach ($project_categories as $cat) {
		
		$jumplink = strtolower(str_replace(' ', '', $cat->name));
		
		echo "<div class='list-content-category'>";
		echo "<h2 id='$jumplink'>$cat->name</h2></div>"; // sub heading

		echo "<div class='list-content-jumplinks'>";
		
		if ( 'preparedcommunities' == strtolower($jumplink) ) {
				echo "<a href='#healthyhabitats'>Healthy Habitats</a><a href='#cleanwater'>Clean Water</a>";
		} elseif ( 'healthyhabitats' == strtolower($jumplink) ) {
				echo "<a href='#preparedcommunities'>Prepared Communities</a><a href='#cleanwater'>Clean Water</a>";
		} elseif ( 'cleanwater' == strtolower($jumplink) ) {
				echo "<a href='#preparedcommunities'>Prepared Communities</a><a href='#healthyhabitats'>Healthy Habitats</a>";
		}
				
		echo "</div>";

		$args = array(
			'post_type' => 'nerra_project',
 			'orderby'    => 'name',
 			'order'		=> 'asc',
 			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'nerra_project_categories',
					'field'    => 'term_taxonomy_id',
					'terms'    => $cat->term_taxonomy_id,
				),
			),
		);
		
		$loop = new WP_Query( $args );
		
		if( $loop->have_posts() ) {

			while( $loop->have_posts() ) : $loop->the_post();
					
				$title = get_the_title();
				$url = get_permalink();
				$description = get_field('project_short_description');
				echo "<div class='list-content-link'>";
				echo '&#149; <a href="' . $url . '">' . $title . '</a>';
				echo "</div>";

			endwhile;
		} 
	} // foreach

	echo "</div>"; // .list-content 
	
}


genesis();


