<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Template Name: Give Today
* Description: A custom template
*/

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_givetoday_page');

function nerra_display_givetoday_page() {
	
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));
		
	global $post;
	
	while ( have_posts() ) : the_post();

		//$custom_fields = get_post_custom(); // get all custom fields
		
		$title = get_the_title();
		$text = get_the_content();
		
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1><h2></h2>
						<div class='text'>";
							the_content();
				//echo "<a class='paypal' href='#'><img src='$path_to_resources/images/site/Give_PayPal_Button.png' width='274' height='40' alt='Give today'></a>";
				// PAYPAL BUTTON
				echo "
				<form class='paypal' action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top'>
				<input type='hidden' name='cmd' value='_s-xclick'>
				<input type='hidden' name='hosted_button_id' value='5SXTC6MJFFN52'>
				<input type='image' src='/wp-content/themes/nerra-ark/images/site/Give_PayPal_Button.png' border='0' name='submit' alt='PayPal - 				The safer, easier way to pay online!'>
				<img alt='' border='0' src='https://www.paypalobjects.com/en_US/i/scr/pixel.gif' width='1' height='1'>
				</form>
				";
			echo "</div></header>";
		echo "</div>
					<div class='image'>";	
						the_post_thumbnail( 'full' );
		echo "</div>";
		echo "</section>";
		
	endwhile;

}



genesis();

