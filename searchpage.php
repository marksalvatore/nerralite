<?php
/*
Template Name: Search Page
*/
?>

<?php $path_to_resources = dirname(get_bloginfo('stylesheet_url')); ?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

	<section class='overview'>
		<div class='group'>
			<header><h1>Search the site</h1></header>
		</div>
		<div class='image'>
			<img src='<?php echo $path_to_resources;?>/images/site/News_Sunset.png' width='352' height='148' alt='News from around the system'>
		</div>
	</section>

	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' );?> ">
	    <div><?php // <label class="screen-reader-text" for="s">Search for:</label> ?>
	        <input type="text" value="" name="s" id="searchbox" />
	        <input type="submit" id="searchsubmit" value="Search" />
	    </div>
	</form>


	</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
