<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: HWW Education Projects
 *
 */

// Referred to as the 'gallery page' for education

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_hwweducation_overview');
add_action('genesis_loop', 'nerra_display_hwweducation_issueblocks');
add_filter( 'body_class','nerra_addclass_howwework' );

function nerra_display_hwweducation_overview() {
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
						<div class='group'>
							<header><h1>";
								the_title();
				echo "</h1></header>
							<div class='text'>";
								
				echo "</div>";
			echo "</div>
						<div class='image'>";	
							
			echo "</div>";
		echo "</section>";

	endwhile;
}
function nerra_display_hwweducation_issueblocks() {
	
	$issues = array( 
				array ( 'page_id' => '1312', 'title' => 'Teachers on the Estuary = Climate Science in the Classroom', 
								'description' => 'Money does not grow on trees, but it could be growing in our coastal salt marshes.'),				
				array ( 'page_id' => '1315', 'title' => 'Citizen science powers Alaska’s Kachemak reserve',
								'description' => 'Every year, volunteers scoop up water samples, monitor crab pots, brave the mud with a GPS—all in the name of advancing local science.'),
				array ( 'page_id' => '1320', 'title' => 'Virginia reserve boosts local awareness of climate change',
								'description' => 'Improving climate literacy and the curriculum with local data.'),
				array ( 'page_id' => '1325', 'title' => 'Hands-on training inspires STEM Curriculum in New Jersey',
								'description' => 'Teachers get hands-on opportunities to learn about ocean currents and other aspects of coastal science.'),

				array ( 'page_id' => '1332', 'title' => 'Florida reserve takes learning underwater.', 
								'description' => 'Florida students got a firsthand look at the underworld of estuaries, while learning how to manage an ROV'),		
				array ( 'page_id' => '1335', 'title' => 'Faith and science converge at the Hudson River reserve',
								'description' => 'Partnering with the Earth Faith Peace project brings people from many faiths together to nurture environmental stewardship.'),
				array ( 'page_id' => '1339', 'title' => 'Washington students say “YES” to stewardship',
								'description' => 'Regional conference encourages high school students to become life-long environmental stewards.'),
				array ( 'page_id' => '1342', 'title' => 'Wells reserve gives children "head start" in nature',
								'description' => 'More than 35 programs helped hundreds of children, educators, and families experience the great outdoors and make exciting wildlife discoveries.'),
			);

	// array of pageids for the 8 issues you want to feature
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));
	$issue_images = array ( 'HWW_Education_1_tn.png', 'HWW_Education_2_tn.png', 'HWW_Education_3_tn.png', 'HWW_Education_4_tn.png', 'HWW_Education_5_tn.png', 'HWW_Education_6_tn.png', 'HWW_Education_7_tn.png', 'HWW_Education_8_tn.png' );
	
	echo "<section class='blocks'>";
		echo "<div class='group'>";
		
		$i = 0;


		foreach ( $issues as $issue ) {	
			
			if ( 0 == $i || 4 == $i ) { 
				echo "<div class='row'>";
					echo "<div class='one-fourth first'>"; 
			}	else {
					echo "<div class='one-fourth'>"; 				
			}
			
			echo "<div class='issue' id='".$issue['page_id']."'>";
			
				echo "<div class='issue-title'>";
					echo "<a href='index.php?page_id=".$issue['page_id']."'><div class='title'>".$issue['title']."</div>";
					echo "<img src='$path_to_resources/images/site/$issue_images[$i]' width='210' height='162' alt='".$issue['title']."'></a>";
				echo "</div>"; //.issue-title
			
				echo "<div class='issue-description'>";				
					echo "<a href='index.php?page_id=".$issue['page_id']."'>";
						echo "<div class='title'>".$issue['title']."</div>";
						echo "<div class='description'>".$issue['description']."</div>";								
					echo "</a>";
					echo "<a class='link-indicator'>Learn more</a>";	
				echo "</div>"; //.issue-descripton
			echo "</div>"; //.issue				
		echo "</div>"; //.one-fourth 
			
			if ( 3 == $i || 7 == $i ) { 
				echo "</div>"; //.row
			}
			$i++;
			
		}	

	echo "</div></section>"; //.group .blocks
}






genesis();


