<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: CPToolkit Doing
 *
 */


remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_hwwtoolkit');
add_filter( 'body_class','nerra_addclass_howwework' );
add_filter( 'body_class','nerra_addclass_page_toolkit' );

function nerra_display_hwwtoolkit() {
	
	while ( have_posts() ) : the_post();
	
		$title = get_the_title();
		
		// Overview
		echo "<section class='overview'>
						<header>
							<h1>Doing</h1>
						</header>";
			echo "<div class='image'>";	
				echo "<a href='/how-we-work/collaborative-project-toolkit/'><img src='/wp-content/uploads/2015/06/Toolkit_ToolboxBig.png' alt='Collaborative Project Toolkit'></a>";	
			echo "</div>";
		echo "</section>";

		if ( is_page('choosing-your-stakeholder-engagement-strategies') ||
				 is_page('stakeholders-engagement-survey') )	
		{
				// special background color for this tool
				echo "<div class='overview-text-background'>";
		}	else {
				echo "<div class='overview-text'>";
		}
		
		if ( ! is_page('doing') ) {
			echo "<h2>$title</h2>";
			if ( is_page('stakeholders-engagement-survey')) {
				echo "<h3 class='survey-report hide'>Stakeholder engagement strategies that could work for you</h3>";
			}
		}
						
		the_content();
		
		if ( is_page('stakeholders-engagement-survey')) {
			echo "<span class='survey-report hide'>Based on the answers you gave, we think the following engagement strategies might be right for your project. You can also go directly to an <a class='internal-link' href='/how-we-work/collaborative-project-toolkit/choosing-your-stakeholder-engagement-strategies/'>overview</a> of the most commonly used stakeholder engagement strategies.</span>";
		}
		

		if ( ! is_page('doing') && ! is_page('stakeholders-engagement-survey') && ! is_page('choosing-your-stakeholder-engagement-strategies')) {
			// jump links
			nerra_display_toolkit_jumplinks();
		}

		echo "</div>"; // overview-text
	
	endwhile;
	
	// Each subsection has a "termid" used to query 
	if ( is_page('manage-your-project') )   {
		nerra_display_toolkit_manageyourproject();
	}
	if ( is_page('collaborate-with-stakeholders') )   {
		nerra_display_toolkit_collaboratewithstakeholders();
	}
	if ( is_page('communicate') )   {
		nerra_display_toolkit_communicate();
	}
	if ( is_page('choosing-your-stakeholder-engagement-strategies') )   {
		nerra_display_toolkit_strategy_matrix();
	}
	if ( is_page('stakeholders-engagement-survey') )   {
		 nerra_display_toolkit_survey_form();
	}
}

function nerra_display_toolkit_manageyourproject(){
	
	$termid = 91; // category id for this subsection
	
	echo "<div class='hr'></div>";
	echo "<div class='toolkit-content'>";
	
		echo "<h3 id='bestpractices'>Best Practices</h3>";
		echo "<div class='intro-text'>";
		echo "The following best practices were assembled to help you coordinate your project, 
					collaborate and communicate with your team, and adapt to unanticipated events and ideas 
					through formative evaluation and risk management.";
		echo "</div>"; //.intro-text
					$doctype = 'best practice'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
					
		echo "<div class='hr'></div>";
		
		echo "<h3 id='casestudies'>Case Studies</h3>";
		echo "<div class='intro-text'>";
		echo "Figuring out how to prepare for and adapt to unexpected events will make your project 
					and your team more resilient. The case studies in this section illustrate how reserve-based 
					collaborative projects responded to two kinds of unanticipated changes.";
		echo "</div>"; //.intro-text
					$doctype = 'case study'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

		echo "<div class='hr'></div>";
		
		echo "<h3 id='resources'>Resources</h3>";
		echo "<div class='intro-text'>";
		echo "Resources in this section can help you develop project management skills—or recognize 
					what you need someone else to do—assess how your team feels about collaborative technologies, 
					and manage project risk.";
		echo "</div>"; //.intro-text
					$doctype = 'resource'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

	echo "</div>"; // .toolkit-content	
	
}
function nerra_display_toolkit_collaboratewithstakeholders(){
	
	$termid = 92; // category id for this subsection
	
	echo "<div class='hr'></div>";
	echo "<div class='toolkit-content'>";
	
		echo "<h3 id='bestpractices'>Best Practices</h3>";
		echo "<div class='intro-text'>";
		echo "Best practices in this section were selected to help you engage your stakeholders, 
					run productive meetings, and deal with conflict.";
		echo "</div>"; //.intro-text
					$doctype = 'best practice'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
					
		echo "<div class='hr'></div>";
		
		echo "<h3 id='casestudies'>Case Studies</h3>";
		echo "<div class='intro-text'>";
		echo "Case studies in this section illustrate different project team experiences with 
					running workshops, engaging younger stakeholders, managing conflict, and keeping 
					the project moving forward between meetings.";
		echo "</div>"; //.intro-text
					$doctype = 'case study'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

		echo "<div class='hr'></div>";
		
		echo "<h3 id='resources'>Resources</h3>";
		echo "<div class='intro-text'>";
		echo "Tools and other resources in this section can help you engage with stakeholders, 
					plan and execute productive meetings, and manage conflict.";
		echo "</div>"; //.intro-text
					$doctype = 'resource'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

	echo "</div>"; // .toolkit-content	
	
}

function nerra_display_toolkit_communicate(){
	
	$termid = 93; // category id for this subsection
	
	echo "<div class='hr'></div>";
	echo "<div class='toolkit-content'>";
	
		echo "<h3 id='bestpractices'>Best Practices</h3>";
		echo "<div class='intro-text'>";
		echo "";
		echo "</div>"; //.intro-text
					$doctype = 'best practice'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);
					
		echo "<div class='hr'></div>";
		
		echo "<h3 id='casestudies'>Case Studies</h3>";
		echo "<div class='intro-text'>";
		echo "Case studies in this section illustrate the complexity of communication 
					within a collaborative project and methods project teams and stakeholders 
					can utilize to arrive at a common language.";
		echo "</div>"; //.intro-text
					$doctype = 'case study'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

		echo "<div class='hr'></div>";
		
		echo "<h3 id='resources'>Resources</h3>";
		echo "<div class='intro-text'>";
		echo "Tools and other resources in this section can help you plan your communications 
					strategy, frame your messages, and enhance your team’s ability to communicate effectively with diverse audiences.";
		echo "</div>"; //.intro-text
					$doctype = 'resource'; // document toolkit type
					$loop = nerra_get_documents_by_termid($termid); // frame your project
					nerra_display_toolkit_documents($loop, $doctype);

	echo "</div>"; // .toolkit-content	
	
}

function nerra_display_toolkit_strategy_matrix(){
	
	echo "<div id='cptool-strategy'>";
	echo "
<table>
<thead>
<tr>
<th>Strategy</th>
<th>WHEN TO<br />DO IT</th>
<th>IDEAL<br />NUMBER OF<br />PARTICIPANTS</th>
<th>WHY DO IT</th>
<th>Conflict Level</th>
</tr>
</thead>
<tbody>
<tr>
<td><a class='internal-link' href='/nerra-strategies/surveys/'>Surveys</a></td>
<td>Any time</td>
<td align='center'>20+</td>
<td>Gather feedback</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/workshops/'>Workshops</a></td>
<td>Any time</td>
<td align='center'>20+</td>
<td>Share information,
gather feedback,
identify options, make
decisions</td>
<td align='center'>Low</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/social-media/'>Social media</a></td>
<td>Any time</td>
<td align='center'>20+</td>
<td>Share information,
gather feedback</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/citizen-science/'>Citizen science</a></td>
<td>Any time</td>
<td align='center'>20+</td>
<td>Gather feedback</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/experiential-engagement/'>Experiential engagement</a></td>
<td>Any time</td>
<td align='center'>10-19</td>
<td>Share information</td>
<td align='center'>Medium</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/focus-groups/'>Focus groups</a></td>
<td>Any time</td>
<td align='center'>2-9</td>
<td>Gather feedback, come up with options</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/gallery-walk/'>Gallery walk</a></td>
<td>Any time</td>
<td align='center'>20+</td>
<td>Gather feedback,
share information, build
alliances</td>
<td align='center'>Medium</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/graphic-facilitation/'>Graphic facilitation</a></td>
<td>Any time</td>
<td align='center'>20+</td>
<td>Gather feedback, come up with options</td>
<td align='center'>Medium</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/keypad-polling/'>Keypad polling</a></td>
<td>Any time</td>
<td align='center'>20+</td>
<td>Gather feedback, make a decision</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/open-house/'>Open House</a></td>
<td>Any time</td>
<td align='center'>20+</td>
<td>Share information</td>
<td align='center'>Medium</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/role-play/'>Role play</a></td>
<td>Any time</td>
<td align='center'>2-9</td>
<td>Share information, identify options, build alliances</td>
<td align='center'>Medium</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/samoan-circle/'>Samoan Circle</a></td>
<td>Any time</td>
<td align='center'>20+</td>
<td>Gather feedback, build alliances</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/design-charette/'>Design charette</a></td>
<td>Beginning</td>
<td align='center'>20+</td>
<td>Gather feedback, come up with options</td>
<td align='center'>Medium</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/key-informant-interviews/'>Key informant interviews</a></td>
<td>Beginning</td>
<td align='center'>2-9</td>
<td>Gather feedback</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/kitchen-table-discussion'>Kitchen table discussion</a></td>
<td>Beginning</td>
<td align='center'>2-9</td>
<td>Gather feedback,
identify options, build
alliances</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/stakeholder-advisory-committee/'>Stakeholder advisory committee</a></td>
<td>Beginning</td>
<td align='center'>2-9</td>
<td>Gather feedback,
identify options, make
decisions</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/technical-advisory-committee/'>Technical advisory committee</a></td>
<td>Beginning</td>
<td align='center'>2-9</td>
<td>Gather feedback,
identify options, make
decisions</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/visioning/'>Visioning</a></td>
<td>Beginning</td>
<td align='center'>20+</td>
<td>Share information,
gather feedback,
identify options, make
decisions</td>
<td align='center'>Medium</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/participatory-mapping/'>Participatory mapping</a></td>
<td>Middle</td>
<td align='center'>20+</td>
<td>Gather feedback,
identify options</td>
<td align='center'>High</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/beta-testing/'>Beta testing</a></td>
<td>End</td>
<td align='center'>10-19</td>
<td>Gather feedback</td>
<td align='center'>Low</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/technical-assistance/'>Technical assistance</a></td>
<td>End</td>
<td align='center'>2-9</td>
<td>Share information</td>
<td align='center'>Medium</td>
</tr>
<tr>
<td><a class='internal-link' href='/nerra-strategies/tools-cafe/'>Tools café</a></td>
<td>End</td>
<td align='center'>20+</td>
<td>Share information</td>
<td align='center'>Medium</td>
</tr>
</tbody>
</table>
</div>
";
	
}

function nerra_display_toolkit_survey_form(){
	echo "<div class='toolkit-content-survey'>";
	echo "<h3>Survey</h3>";

	// survey form
	echo "
	<form action='here' method='post' id='survey-form'>
		<div class='form'>
		
			<div class='question'>
				<div class='title'>
					Question 1.  How many people do you want to reach with this strategy?
					<span>Select the answer that best matches your goal from the list below.</span>
				</div>
				<div class='buttongroup'>
					<label><input type='radio' name='howmany' value='1'>
						<span>Small: 2 - 9</span></label>
					<label><input type='radio' name='howmany' value='2'>
						<span>Medium: 10 - 19</span></label>
					<label><input type='radio' name='howmany' value='3'>
						<span>Large: 20 +</span></label>
					<label><input type='radio' name='howmany' value='4'>
						<span>Not sure</span></label>
				</div>
			</div>
	
			
			<div class='question'>
				<div class='title'>
					Question 2. How much conflict or disagreement is there among your stakeholders 
					about important aspects of your project?
					<span>Select the answer that best matches your assessment from the list below.</span>
				</div>
				<div class='buttongroup'>
					<label><input type='radio' name='howmuch' value='1'>
						<span>High <div>There are significant disagreements or bad feelings about the 'right' 
						information to include or next steps to take.</div></span></label>
					<label><input type='radio' name='howmuch' value='2'>
						<span>Medium<div>There are some known concerns and disagreements, 
						but generally stakeholders are able to have productive discussions.</div></span></label>
					<label><input type='radio' name='howmuch' value='3'>
						<span>Low<div>There are few or disagreements about how to proceed, and stakeholders 
						feel empowered vis a vis the project.</div></span></label>
					<label><input type='radio' name='howmuch' value='4'>
						<span>Not sure</span></label>
				</div>
			</div>
	
			<div class='question'>
				<div class='title'>
					Question 3. What do you want to accomplish with this group?
					<span>Check all that apply.</span>
				</div>
				<div class='buttongroup'>
					<label><input type='checkbox' id='what_1' value='1'>
						<span>Gather feedback</span></label>
					<label><input type='checkbox' id='what_2' value='2'>
						<span>Come up with options / alternatives</span></label>
					<label><input type='checkbox' id='what_3' value='3'>
						<span>Make a decision</span></label>
					<label><input type='checkbox' id='what_4' value='4'>
						<span>Share information</span></label>
					<label><input type='checkbox' id='what_5' value='5'>
						<span>Build alliances</span></label>
				</div>
			</div>
	
			<div class='question'>
				<div class='title'>
					Question 4. At what point in the project do you want to do this?
					<span>Select the answer that best matches your goal from the list below.</span>
				</div>
				<div class='buttongroup'>
					<label><input type='radio' name='when' value='1'>
						<span>At the beginning (usually involving scoping and planning activities)</span></label>
					<label><input type='radio' name='when' value='2'>
						<span>In the middle (usually involving research, information creation, or analysis activities)</span></label>
					<label><input type='radio' name='when' value='3'>
						<span>At the end (usually involving implementation, communication, and evaluation activities)</span></label>
					<label><input type='radio' name='when' value='4'>
						<span>Not sure</span></label>
				</div>
			</div>
			
			<a class='submitsurvey' href='#'><img src='/wp-content/uploads/2015/08/Toolkit_SubmitButton.png' width='216' height='30' alt='submit button'></a>
		</div>
	</form>
	"; // end form
	
	// Strategy summaries
	nerra_display_strategy_summaries();

	echo "</div>"; //.toolkit-content-survey
}

function nerra_display_strategy_summaries(){

	$args = array(
		'post_type' => 'nerra_strategies',
		'orderby'   => 'meta_value_num',
		'meta_key'  => 'strategy_sort_order',
		'order'		=> 'asc',
		'posts_per_page' => -1,
	);
	
	$loop = new WP_Query( $args );
	
	if ( $loop ) {
		if( $loop->have_posts() ) {
			
			echo "<div class='strategies hide'>";
			echo "[ <a class='link-indicator' href='/how-we-work/collaborative-project-toolkit/stakeholders-engagement-survey/'>back to survey</a> ]";
			
			while( $loop->have_posts() ): $loop->the_post();
				//$postid = get_the_ID();
				$title = get_the_title();
				$text = get_field('strategy_summary');
				//$slug = $loop->post_name;
				$slug = strtolower(str_replace(' ', '-', $title));
				$slug = strtolower(str_replace('é', 'e', $slug));

				echo "<div class='$slug'>";
					echo "<h2>$title</h2>";
					echo "<p>$text</p><span class='arrow-bullet-alt'></span> <span>Read more about <a class='internal-link' href='/nerra-strategies/$slug'>$title</a></span>";				
				echo "</div>";

			endwhile;	
			
			echo "</div>"; // .strategies
		}		
	}
	wp_reset_postdata();
}


genesis();