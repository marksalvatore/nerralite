<?php
/**
 * Template Name: 404
 *
 */


//* Remove default loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_loop', 'genesis_404' );
/**
 * This function outputs a 404 "Not Found" error message
 *
 * @since 1.6
 */
function genesis_404() {

	echo '<article class="entry">';

		printf( '<h1 class="entry-title">%s</h1>', __( "Oops! We couldn't find that page.", 'genesis' ) );
		echo '<div class="entry-content">';

				echo '<p>' . sprintf( __( 'The page you are looking for no longer exists. Perhaps you can return back to the <a href="%s">homepage</a> and see if you can find what you are looking for. Or, you can try finding it by using the search form below.', 'genesis' ), home_url() ) . '</p>';


				nerra_display_search_form();


			echo '</div>';

		echo '</article>';

}

genesis();
