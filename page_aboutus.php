<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: About Us
 *
 */

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_aboutus_page');
add_filter( 'body_class','nerra_addclass_aboutus' );
add_filter( 'body_class','nerra_addclass_page_primary' );


function nerra_display_aboutus_page() {
	
	global $post;
	
	$custom_fields = get_post_custom(); // get all custom fields

	$story_text = $custom_fields['story_text'];
	$story_image = $custom_fields['story_image'];
	$story_quote_text = $custom_fields['story_quote_text'];
	$story_quote_authorname = $custom_fields['story_quote_authorname'];
	$story_quote_authorplace = $custom_fields['story_quote_authorplace'];
	$quote1_image = $custom_fields['quote1_image'];
	$quote1_text = $custom_fields['quote1_text'];
	$quote1_authorname = $custom_fields['quote1_authorname'];
	$quote1_authorplace	 = $custom_fields['quote1_authorplace'];
	$quote2_image = $custom_fields['quote2_image'];
	$quote2_text = $custom_fields['quote2_text'];
	$quote2_authorname = $custom_fields['quote2_authorname'];
	$quote2_authorplace = $custom_fields['quote2_authorplace'];

	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
					<div class='group'>
						<header><h1>";
							the_title();
			echo "</h1></header>
						<div class='text'>";
							the_content();
			echo "</div>";
		echo "</div>
					<div class='image'>";	
						the_post_thumbnail( 'full' );
		echo "</div>";
		echo "</section>";


		// Story
		echo "<section class='story'>";
			echo "<div class='text'>".$story_text[0]."</div>";				
		echo "</section>";
		// The image group is outside the section so it overlaps the section's background
			echo "<div class='story-group'>";
				echo "<div class='image'>".wp_get_attachment_image($story_image[0], 'full')."</div>";
				echo "<div class='quote'><q>".$story_quote_text[0]."</q><br />";
					echo "<div class='author'>".$story_quote_authorname[0].", </div><div class='place'>".$story_quote_authorplace[0]."</div>";
				echo "</div>";			
			echo "</div>";


		// Quotes
		echo "<section class='quotes'>";
		
			echo "<div class='one-half first'>";		
				echo "<div class='image'>".wp_get_attachment_image($quote1_image[0], 'full')."</div>";
				echo "<div class='text'><q>".$quote1_text[0]."</q>";
					echo "<div class='author'>".$quote1_authorname[0]."<div class='place'>".$quote1_authorplace[0]."</div></div>";
				echo "</div>";
			echo "</div>";

			echo "<div class='one-half'>";		
				echo "<div class='image'>".wp_get_attachment_image($quote2_image[0], 'full')."</div>";
				echo "<div class='text'><q>".$quote2_text[0]."</q>";
					echo "<div class='author'>".$quote2_authorname[0]."<div class='place'>".$quote2_authorplace[0]."</div></div>";
				echo "</div>";				
			echo "</div>";
				
		echo "</section>";


	endwhile;
	
}






genesis();