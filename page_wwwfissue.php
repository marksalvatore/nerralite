<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Template Name: WWWF Issue
* Description: Custom page
*/

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_wwwfissue_page');
add_filter( 'body_class','nerra_addclass_whatweworkfor' );

function nerra_display_wwwfissue_page() {
	
	global $post;
	
	
	while ( have_posts() ) : the_post();

		$custom_fields = get_post_custom(); // get all custom fields
		
		$why_text = $custom_fields['issue_why_text'];
		$why_image = $custom_fields['issue_why_image'];
		$how_text = $custom_fields['issue_how_text'];
		$featured_project_name = get_field('issue_featured_project_name');
		$featured_project_image = get_field('issue_featured_project_image');
		$featured_project_link = get_field('issue_featured_project_link');
		$what_text = $custom_fields['issue_what_text'];
		$what_image = $custom_fields['issue_what_image'];		
		 
		$title = get_the_title();
		$text = get_the_content();
		
		// Overview
		echo "<section class='overview'>
						<div class='group'>
							<header><h1>$title</h1></header>							
							<ul class='jump-menu'><li><a href='#itmatters'>Why it matters</a></li>
								<li><a href='#wehelp'>How we help</a></li>
								<li><a href='#youcan'>What you<br />can do</a></li>
							</ul>
							<div class='text'><p>$text</p></div>		
						</div>"; //group
						
			echo "<div class='image'>";
		//* TEMPORARILY removing hottopic from display per DL 08/19/2015
							the_post_thumbnail( 'full' );		
				echo "<div class='caption'>Hot Topics</div>";
						//echo "<a href='#'>";
							//echo "<div class='title'>Coastal blue carbon</div>";
							//echo "<div class='readmore'>Read more</div>";
							echo "<div class='readmore'>Coming soon!</div>";
						//echo "</a>";
						//echo "</div>"; //image

		echo "</section>";


		// Why It Matters
		echo "<section class='why-section' id='itmatters'>
						<div class='group'>
							<h2>Why it matters</h2>
							<div class='text'><p>".$why_text[0]."</p></div>";	
			echo "</div>
						<div class='why-section-image'>".wp_get_attachment_image($why_image[0], 'full')."</div>"; 
		echo "</section>";

		// How We Help
		echo "<section class='how-section' id='wehelp'>
						<div class='group'>
							<h2>How we help</h2>";
				echo "<div class='text'><p>".$how_text[0]."</p></div>";
			echo "</div>"; //.group
			echo "<div class='featured'>";
				
					echo "<div class='featured-text'>";
						echo "<h3>Featured Project</h3>";
						echo "<div class='text'>";
								echo $featured_project_name;
								echo "<a class='link-indicator newline' href='$featured_project_link'>Read More</a>";
						echo "</div>";
					echo "</div>"; //.featured-text				
					echo "<a href='$featured_project_link'><img width='177' height='177' class='featured-image' src='".$featured_project_image['url']."' alt='$featured_project_name' /></a>";	
			echo "</div>"; //.featured
				
		echo "</section>";

		echo "<div class='hr'></div>";

		echo "<section class='what-section' id='youcan'>
						<div class='group'>
							<h2>What you can do</h2>
							<div class='text'><p>".$what_text[0]."</p></div>	
						</div>
						<div class='image'>".wp_get_attachment_image($what_image[0], 'full')."</div>		
					</section>";
					
	endwhile;

}



genesis();

