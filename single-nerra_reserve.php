<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
* Single Post Template: NERRA Reserve
* Description: Custom page
*/

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_reserve_page');

function nerra_display_reserve_page() {
	
	$template_path = dirname(get_bloginfo('stylesheet_url'));
		
	while ( have_posts() ) : the_post();
				
		$reserve_name = get_the_title();
		$reserve_shortname = get_field('reserve_shortname');
		$reserve_url = get_field('reserve_url');
		$reserve_description = get_field('reserve_description');
		$reserve_image = get_field( 'reserve_image' );
		$reserve_logo = get_field( 'reserve_logo' );
		$reserve_boundary_map = get_field( 'reserve_boundary_map' );
		
		// facebook needs a username
		// twitter needs a username and widget-id (linked to account as gateway permitting only one stream)
		
		$facebook_username = get_field('reserve_facebook_username');
		$facebook_page = get_field('reserve_facebook_page_url');
		$twitter_username = get_field('reserve_twitter_username');
		$twitter_widget_id = get_field('reserve_twitter_widget_id'); //  600724936517242880
		$instagram_username = get_field('reserve_instagram_username');
		
		$vitals_state = get_field('reserve_vitals_state');
		$vitals_state_partner = get_field('reserve_vitals_state_partner');
		$vitals_habitat = get_field('reserve_vitals_habitat');
		$vitals_year_designated = get_field('reserve_vitals_year_designated');
		$vitals_acreage = get_field('reserve_vitals_acreage');
		$vitals_profile_pdf = get_field('reserve_vitals_profile');
		
		$vitals_image = get_field('reserve_vitals_image');
		$visit_text = get_field('reserve_visit_text');
		$visit_image = get_field('reserve_visit_image');
		$visit_url = get_field('reserve_visit_url');
		$travel_guide_name = get_field('reserve_travel_guide_name');
		$travel_guide_url = get_field('reserve_travel_guide_url');
		$featured_project_name = get_field('reserve_featured_project_name');
		$featured_project_image = get_field('reserve_featured_project_image');
		$featured_project_link = get_field('reserve_featured_project_link');
		$volunteer_text = get_field('reserve_volunteer_text');
		$volunteer_image = get_field('reserve_volunteer_image');
		$volunteer_url = get_field('reserve_volunteer_url');
		$friend_text = get_field('reserve_friend_text');
		$friend_url = get_field('reserve_friend_url');
		$contact_info = get_field('reserve_contactinfo');
		$did_you_know_image = get_field('reserve_did_you_know_image');
		$did_you_know_caption = get_field('reserve_did_you_know_caption');


		// Overview
		echo "<section class='overview'>
						<div class='group'>
							<header><h1>$reserve_name</h1></header>
							<ul class='jump-menu'>									
								<li><a href='#vitals'>Vital stats</a></li>
								<li><a href='#visit'>Visit</a></li>
								<li><a href='#volunteer'>Volunteer</a></li>
								<li><a href='#friend'>Become a friend</a></li>
								<li><a href='#contact'>Contact</a></li>									
							</ul>									
							<div class='text'>$reserve_description";									

								// links after intro text
								if( !empty($reserve_boundary_map)) {
									echo "<a class='link-indicator' href='$template_path/kml/$reserve_boundary_map' target='_blank'>Boundary Map</a>
												<span class='link-divider'></span>";
								}
						echo "<a class='link-indicator external newline' href='$reserve_url' target='_blank'>Visit the Reserve's Website</a>";

				echo "</div>";
			echo "</div>
		
						<div class='image'>";
				echo "<img src=".$reserve_image['url']." />";
			echo "</div>		
					</section><!-- end .overview -->";

		// VITAL
		echo "<span id='vitals'></span>";
		echo "<section class='reserve-vitals'>";
				
				echo "<div class='group'>";
				
					echo "<div class='stats'>";
							echo "<h2>Vital Stats</h2>";
							echo "<ul class='bullet-list'><li> State: $vitals_state</li>";
							echo "<li> State Partner: <strong>$vitals_state_partner</strong></li>";
							echo "<li> Acreage: $vitals_acreage</li>";
							echo "<li> Habitats: $vitals_habitat</li>";
							echo "<li> Designated: $vitals_year_designated</li>";
							
							if ( ! empty($vitals_profile_pdf['url']) ) {
								echo "<li> Site Profile: <a class='internal-link' href=".$vitals_profile_pdf['url'].">View</a></li>";
							}
							
							echo "</ul>"; //.text
					echo "</div>"; //.stats

					echo "<div class='nerrs-datafeed'>";
						echo "<div class='image'>";
							if ( ! empty($vitals_image['url']) ) {
								echo "<img src=".$vitals_image['url']." />";
							} else {
								echo "<div class='photo'><img src='http://placehold.it/177x235'></div>";						
							}
						echo "</div>"; //.image
						echo "<div class='caption'>";
						echo "The $reserve_shortname reserve participates in the System-Wide Monitoring Program (SWMP), which collects, analyzes, and makes available weather, water quality, and nutrient data from all National Estuarine Research Reserves. <a class='link-indicator newline' href='http://cdmo.baruch.sc.edu' target='_blank'>Learn more</a>";
						echo "</div>"; //.caption
					echo "</div>"; //.nerrs-datafeed


					/* SWMP data feed - possible installation later this year. MS::Aug 13, 2015
					require_once( get_stylesheet_directory() . '/lib/nusoap/nusoap.php');
					$wsdl=new nusoap_client('http://cdmo.baruch.sc.edu/webservices2/requests.cfc?wsdl');
					
					$wsdl->call('NERRFilterStationCodesXMLNew');
					
					
					$response = 'Response: <xmp>'.$wsdl->response.'</xmp>';
					$startpos_xml = strpos($response, '<?xml');
					$xml = substr($response, $startpos_xml);
					
					$simple = $xml;//"<para><note>simple note</note></para>";
					$p = xml_parser_create();
					xml_parse_into_struct($p, $simple, $vals, $index);
					xml_parser_free($p);
					//echo "Index array\n";
					//print_r($index);
					echo "\n<pre>Vals array\n";
					print_r($vals);
					/*
					foreach ($vals as $v) {
						if ($v['tag'] == 'NERR_SITE_ID')
							echo 'SiteID: '.$v['value'].'<br>';
					}
					*/
					//*
					
				echo "</div>"; //.group	
				
				echo "<div class='featured'>";
				
					echo "<div class='featured-text'>";
						echo "<h3>Featured Project</h3>";
						echo "<div class='text'>$featured_project_name</div>";
						echo "<a class='link-indicator internal newline' href='$featured_project_link'>Read More</a>";
				
					echo "</div>"; //.featured-text				
					echo "<a href='$featured_project_link'><img class='featured-image' src=".$featured_project_image['url']." /></a>";	
				echo "</div>"; //.featured
		
		echo "</section>"; //.reserve-vitals
			

		// VISIT
		echo "<span id='visit'></span>";
		echo "<section class='reserve-visit'>";		
				echo "<div class='group'>";
						echo "<h2>Visit</h2>";
						echo $visit_text." ";
						
						echo "<div class='paragraph'>";
						if (!empty($visit_url)) {
							echo "<a class='link-indicator' href='$visit_url' target='_blank'>Learn&nbsp;More</a>";
						}
						if ( ! empty($travel_guide_name) && ! empty($travel_guide_url) ) {
							echo "<span class='link-divider'></span><a class='link-indicator' href='$travel_guide_url' target='_blank'>$travel_guide_name</a>";
						}
						echo "</div>";
						if ( ! empty($visit_image['url'])) echo "<img src=".$visit_image['url']."  />";		
				echo "</div>"; //.group

				echo "<div class='socialmedia'>";		
				
				// social media
				if ( ! empty($facebook_username) || ! empty($facebook_page)) {
					nerra_display_socialmedia_boxes($facebook_username, $facebook_page, $twitter_username, $twitter_widget_id);
				} else {
					echo "<div style='padding: 200px 0 0 40px;'>This window to our Facebook page is coming soon!</div>";				
				}
					
				echo "</div>"; //.socialmedia		
		echo "</section>"; //.reserve-visit
		
		echo "<div class='hr'></div>";


		echo "<section class='reserve-volunteer-area'>";
				
			echo "<div class='reserve-volunteerfriends'>";	
	
				// Volunteer
				echo "<span id='volunteer'></span>";
				echo "<div class='reserve-volunteer'>";	

				
					echo "<div class='group'>";
						echo "<h2>Volunteer</h2>";		
						echo "<div class='text'>";
						if ( !empty($volunteer_text) ) {
							echo "<p>$volunteer_text";
							if ( !empty($volunteer_url)) echo "<a class='link-indicator newline' href='$volunteer_url' target='_blank'>Learn&nbsp;more</a>";
						} else {
							echo "<p>This reserve currently does not have a formal volunteer organization."; 
						}
						echo "</p></div>"; //.text
					echo "</div>"; //.group
					
					if ( ! empty($volunteer_image['url'])) echo "<div class='image'><img src=".$volunteer_image['url']." /></div>";
				
				
				echo "</div>"; //.reserve-volunteer
					
				echo "<div class='hr-half'></div>";	
					
				// Become a friend
				echo "<span id='friend'></span>";
				echo "<div class='reserve-friend'>";	
					echo "<h2>Become a Friend</h2>";		
						
					echo "<div class='text'>";
						if ( !empty($friend_text) ) {
							 echo "<p>$friend_text";
							 if ( !empty($friend_url)) echo "<a class='link-indicator newline' href='$friend_url' target='_blank'>Join Today</a>";
						} else {
							echo "<p>This reserve currently does not have a formal friends organization. Please stay tuned for more information."; 
						}
						
					echo "</p></div>"; //.text
					
				echo "</div>"; //.reserve-friend
	
			echo "</div>"; //.reserve-volunteerfriends
		
			// Did you know?
			echo "<div class='reserve-didyouknow'>";
				if ( ! empty($did_you_know_image['url']) ) echo "<div class='image'><img src=".$did_you_know_image['url']." /></div>";
				if ( ! empty($did_you_know_image['url']) ) echo "<div class='text'><h3>Did you know?</h3>$did_you_know_caption</div>";
			echo "</div>";

		echo "</section>"; //.reserve-volunteer-area

			
		echo "<div class='hr'></div>";


		// CONTACT
		echo "<span id='contact'></span>";
		echo "<section class='reserve-contact'>";	
			
			echo "<div class='one-half first'>";
			
				echo "<h2>Contact</h2>";		
				echo "<div class='text'>";	
					if (!empty($contact_info)) echo "<p>$contact_info</p>";
				echo "</div>"; //.text
									
			echo "</div><div class='one-half reserve-logos'>";
						
				if ( ! empty($reserve_logo['url']) ) echo "<div class='image'><img src=".$reserve_logo['url']." /></div>";
						
			echo "</div>"; // end columns								
		echo "</section>"; //.reserve-contact
				
		
	endwhile;
}


genesis();

