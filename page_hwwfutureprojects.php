<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access permitted.' );

/**
 * Template Name: HWW Future Projects
 *
 */

// Referred to as the 'gallery page' for future projects

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'nerra_display_hwwfuture_overview');
add_action('genesis_loop', 'nerra_display_hwwfuture_issueblocks');
add_filter( 'body_class','nerra_addclass_howwework' );

function nerra_display_hwwfuture_overview() {
	
	while ( have_posts() ) : the_post();
	
		// Overview
		echo "<section class='overview'>
						<div class='group'>
							<header><h1>";
								the_title();
				echo "</h1></header>
							<div class='text'>";
								
				echo "</div>";
			echo "</div>
						<div class='image'>";	
							
			echo "</div>";
		echo "</section>";

	endwhile;
}
function nerra_display_hwwfuture_issueblocks() {
	
	$issues = array( 
				array ( 'page_id' => '1651', 'title' => 'Lessons from a superstorm prepare Mainers for the future', 
								'description' => 'Hurricane Sandy changed New Jersey’s approach to preparedness; will it do the same for Maine?'),				
				array ( 'page_id' => '1659', 'title' => 'Farming a return to the wild at Elkhorn Slough',
								'description' => 'Farm fields become coastal prairie as part of the Tidal Wetlands Project.'),
				array ( 'page_id' => '1661', 'title' => 'Information to manage a changing bay',
								'description' => 'Sentinel site program tracks the impacts of climate change.'),
				array ( 'page_id' => '1665', 'title' => 'Blue Carbon heats up in the Pacific Northwest',
								'description' => 'Blue could be the new green for the region’s salt marsh management.'),
								
				array ( 'page_id' => '1841', 'title' => 'Living shorelines for the Georgia Coast', 
								'description' => 'Innovative approaches to shoreline stabilization balance the needs of the public and natural systems.'),		
				array ( 'page_id' => '1848', 'title' => 'Restoring wetlands at the Prime Hook Wildlife Refuge',
								'description' => 'Engaging stakeholders around the refuge’s wetland restoration strategies and challenges.'),
				array ( 'page_id' => '1852', 'title' => 'Free pass for trout in Branch Brook',
								'description' => 'Partnership restores access to miles of habitat for a diverse assortment of aquatic life.'),
				array ( 'page_id' => '1923', 'title' => 'Seagrass monitoring in Apalachicola Bay',
								'description' => 'Submerged aquatic vegetation may be out of site, but not out of mind for the Apalachicola reserve.'),
			);

	// array of pageids for the 8 issues you want to feature
	$path_to_resources = dirname(get_bloginfo('stylesheet_url'));
	$issue_images = array ( 'HWW_Future_1_tn.png', 'HWW_Future_2_tn.png', 'HWW_Future_3_tn.png', 'HWW_Future_4_tn.png', 'HWW_Future_5_tn.png', 'HWW_Future_6_tn.png', 'HWW_Future_7_tn.png', 'HWW_Future_8_tn.png' );
	
	echo "<section class='blocks'>";
		echo "<div class='group'>";
		
		$i = 0;


		foreach ( $issues as $issue ) {	
			
			if ( 0 == $i || 4 == $i ) { 
				echo "<div class='row'>";
					echo "<div class='one-fourth first'>"; 
			}	else {
					echo "<div class='one-fourth'>"; 				
			}
			
			echo "<div class='issue' id='".$issue['page_id']."'>";
			
				echo "<div class='issue-title'>";
					echo "<a href='index.php?page_id=".$issue['page_id']."'><div class='title'>".$issue['title']."</div>";
					if ( ! empty($issue_images[$i]) ) {
						echo "<img src='$path_to_resources/images/site/$issue_images[$i]' width='210' height='162' alt='".$issue['title']."'></a>";
					}
				echo "</div>"; //.issue-title
			
				echo "<div class='issue-description'>";				
					echo "<a href='index.php?page_id=".$issue['page_id']."'>";
						echo "<div class='title'>".$issue['title']."</div>";
						echo "<div class='description'>".$issue['description']."</div>";								
					echo "</a>";
					echo "<a class='link-indicator'>Learn more</a>";	
				echo "</div>"; //.issue-descripton
			echo "</div>"; //.issue				
		echo "</div>"; //.one-fourth 
			
			if ( 3 == $i || 7 == $i ) { 
				echo "</div>"; //.row
			}
			$i++;
			
		}	

	echo "</div></section>"; //.group .blocks
}






genesis();


